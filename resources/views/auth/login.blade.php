@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-sm-8 col-md-6">
            <div class="card">
                <div class="card-header p-3">
                    <h3 class="mb-0"><i class="fas fa-sign-in-alt"></i> {{ __('Login') }}</h3>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-prepend" style="min-width: 42px">
                                        <span class="input-group-text border-0 bg-white"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm border-0 shadow-none @error('usuario') is-invalid @enderror" placeholder="Usuário" name="usuario" value="{{ old('usuario') }}" required autocomplete="usuario" autofocus data-toggle="tooltip" title="Entre com seu usuário Active Directory.">
                                </div>
                                @error('usuario')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text border-0 bg-white"><i class="fas fa-lock-open"></i></span>
                                    </div>
                                    <input id="password" type="password" class="form-control form-control-sm border-0 shadow-none @error('password') is-invalid @enderror" placeholder="Senha" name="password" required autocomplete="current-password" data-toggle="tooltip" title="Entre com a senha do seu usuário Active Directory.">
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit"
                                    class="btn btn-primary btn-sm btn-block"
                                    id="send_login_button"
                                    data-toggle="tooltip"
                                    title="Clique aqui para acessar o sistema.">
                                    <i class="fas fa-sign-in-alt"></i> Entrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="text-center text-info">
                        * Para acessar o sistema informe o mesmo usuário e senha que utiliza para acessar seu computador. Em caso de dúvida entre em contato com o setor de Tecnologia.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

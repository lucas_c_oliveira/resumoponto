@extends('layouts.app')

@section('page-header')
<div class="row">
    <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-1">
        <div class="form-group row mb-0">
            <label for="filial" class="col-lg-4 form-group col-form-label">
                <h4 class="mb-0"><i class="fas fa-check-double"></i> Fechamentos</h4>
            </label>
            <div class="offset-lg-4 col-lg-4 form-group text-center">
                <div class="dropdown btn-block">
                    <button class="btn btn-info btn-sm btn-block dropdown-toggle" type="button" id="periodos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-calendar-alt"></i> {{ $periodos['periodo_selecionado']['mes_extenso'] }} de {{ $periodos['periodo_selecionado']['ano'] }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="periodos" id="periodos-label-dropdown-menu">
                        @foreach ($periodos['periodos'] as $periodo)
                        <a class="dropdown-item @if($periodos['periodo_selecionado']['ano_mes'] == $periodo->ano_mes ) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro para o período de {{ $periodo->mes_extenso }} de {{ $periodo->ano }}.', '{{URL::to('/filtrar_por_periodo')}}?ano_mes={{ $periodo->ano_mes }}')">
                            <i class="fas fa-calendar-alt"></i> {{ $periodo->mes_extenso }} de {{ $periodo->ano }}
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-3">
    <div class="col-md-12">
    @if ($data_fechamento)

    <h3 class="text-center"> A planilha de fechamento está disponível para download.<br>
    Fechamento realizado em
    {{ date('d/m/Y H:i:s', strtotime($data_fechamento)) }}.</h3>

    @else

    <h3 class="text-center">A planilha de fechamento ainda não está disponível. <br>
    O fechamento ainda não foi realizado.</h3>

    @endif
    </div>

    <div class="col-md-12 text-center">
    @if (\Auth::user()->can('eAdministrador', \App\User::class))

        @if ($data_fechamento)

        <div class="btn-group">
            <button type='button' onclick="exporta_loading('Exportando os fechamentos em CSV.', '{{ url('/exporta/fechamentos/csv') }}', 'Fechamentos.csv')" class="btn btn-info btn-sm"><i class="fas fa-file-csv"></i> Exportar Fechamentos em CSV</button>
            <button type="button" onclick="exporta_loading('Exportando os fechamentos em XLS.', '{{ url('/exporta/fechamentos/xls') }}', 'Fechamentos.xls')" class="btn btn-info btn-sm"><i class="fas fa-file-excel"></i> Exportar Fechamentos em XLS</button>
        </div>

        @endif

    @endif
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Relatório de colaboradores transferidos/demitidos</h3>
    </div>
    <div class="col-md-12 text-center">
        <div class="btn-group">
            <button type='button' onclick="exporta_loading('Exportando os transferidos e demitidos em CSV.', '{{ url('/exporta/transferidos/csv') }}', 'Transferidos.csv')" class="btn btn-info btn-sm"><i class="fas fa-file-csv"></i> Exportar Transferidos / Demitidos em CSV</button>
            <button type="button" onclick="exporta_loading('Exportando os transferidos e demitidos em XLS.', '{{ url('/exporta/transferidos/xls') }}', 'Transferidos.xls')" class="btn btn-info btn-sm"><i class="fas fa-file-excel"></i> Exportar Transferidos / Demitidos em XLS</button>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection

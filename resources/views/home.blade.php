@extends('layouts.app')

@section('page-header')
    <div class="row">
        <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-2">
            <div class="form-group row mb-0">
                <label for="filial" class="col-lg-4 form-group col-form-label">
                    <h4 class="mb-0"><i class="fas fa-list-ul"></i> Lançamentos | Filial</h4>
                </label>
                <div class="col-lg-4 form-group text-center">
                    <div class="dropdown btn-block">
                        <button class="btn btn-info btn-sm btn-block dropdown-toggle" type="button" id="filial" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-second="tooltip" title="Clique para selecionar a Filial.">
                            <i class="fas fa-store-alt"></i> {{ $filiais['filial_escolhida']->nr_fil }} - {{ $filiais['filial_escolhida']->filial }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="filial" id="filial-label-dropdown-menu">
                            @foreach ($filiais['filiais'] as $filial)
                            <a class="dropdown-item @if($filiais['filial_escolhida']->nr_fil == $filial->nr_fil) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro para a Filial {{ $filial->nr_fil }} - {{ $filial->filial }}.', '{{URL::to('/filtrar_por_filial')}}?nr_fil={{ $filial->nr_fil }}')">
                                <i class="fas fa-store-alt"></i> {{ $filial->nr_fil }} - {{ $filial->filial }}
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 form-group text-center">
                    <div class="dropdown btn-block">
                        <button class="btn btn-info btn-sm btn-block dropdown-toggle" type="button" id="periodos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-second="tooltip" title="Clique para selecionar o período.">
                            <i class="fas fa-calendar-alt"></i> {{ $periodos['periodo_selecionado']['mes_extenso'] }} de {{ $periodos['periodo_selecionado']['ano'] }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="periodos" id="periodos-label-dropdown-menu">
                            @foreach ($periodos['periodos'] as $periodo)
                            <a class="dropdown-item @if($periodos['periodo_selecionado']['ano_mes'] == $periodo->ano_mes ) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro para o período de {{ $periodo->mes_extenso }} de {{ $periodo->ano }}.', '{{URL::to('/filtrar_por_periodo')}}?ano_mes={{ $periodo->ano_mes }}')">
                                <i class="fas fa-calendar-alt"></i> {{ $periodo->mes_extenso }} de {{ $periodo->ano }}
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    @php
        $funcionario = [];
    @endphp
    <div class="row">
        <form action="{{URL::to('/')}}/" method="POST" style="width: 100%" id="formLancamentos">
            @csrf
            <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-2">
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <h4 class="mb-0 pb-2 border-bottom">Status do Lançamento <span class='text-info'>(Clique em salvar ao fim da página)</span></h4>
                    </div>
                    <div class="col-lg-4 form-group text-center">
                        <div class="form-check form-check-inline" data-toggle="tooltip" title="Clique para marcar lançamento em progresso.">
                            <input type="radio" class="form-check-input" name="status_lanc" value="0" @if ($status_lanc == 0) checked @endif id="status_lanc1">
                            <label for="status_lanc1" class="form-check-label text-dark">Lançamento em progresso</label>
                        </div>
                    </div>
                    <div class="col-lg-4 form-group text-center">
                        <div class="form-check form-check-inline" data-toggle="tooltip" title="Clique para marcar que não haverá lançamentos este mês.">
                            <input type="radio" name="status_lanc" id="status_lanc2" value="1" class="form-check-input" @if($status_lanc == 1) checked @endif>
                            <label for="status_lanc2" class="form-check-label text-dark">Não haverá lançamento este mês</label>
                        </div>
                    </div>
                    <div class="col-lg-4 form-group text-center">
                        <div class="form-check form-check-inline" data-toggle="tooltip" title="Clique para marcar como lançamento realizado.">
                            <input type="radio" name="status_lanc" id="status_lanc3" value="2" class="form-check-input" @if($status_lanc == 2) checked @endif>
                            <label for="status_lanc3" class="form-check-label text-dark">Lançamento realizado</label>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-1 bg-white">
                    <div class="row">
                        <div class="col-lg-3 text-center form-group"><b>Legenda:</b></div>
                        <div class="col-lg-3 bg-primary text-center text-white form-group pl-3 pr-3"><b><i class="far fa-calendar"></i> Mês atual ({{ $periodos['periodo_selecionado']['mes_extenso'] }})</b></div>
                        <div class="offset-lg-1 col-lg-3 bg-secondary text-center form-group pl-3 pr-3"><b><i class="far fa-calendar"></i> Mês anterior ({{ $periodos['periodo_passado']['mes_extenso'] }})</b></div>
                    </div>
                </div>
            </div> --}}
            <table class="table table-sm table-hover table-striped display dt-responsive" id="tabela-lancamentos" width="100%"  cellspacing="0">
                <thead>
                    <tr class="text-center">
                        <th colspan="3" class="border-0" id="complex_header0"></th>
                        <th colspan="2" class="border-0 hide" id="complex_header1"></th>
                        <th colspan="4" class="bg-primary border-bottom text-white" id="complex_header2"><i class="far fa-calendar"></i> Mês atual ({{ $periodos['periodo_selecionado']['mes_extenso'] }})</th>
                        <th colspan="4" class="bg-light border-bottom" id="complex_header3"><i class="far fa-calendar"></i> Mês anterior ({{ $periodos['periodo_passado']['mes_extenso'] }})</th>
                    </tr>
                    <tr class="text-center bg-info">
                        <th class="text-white tablet-l desktop desktop-lg" data-priority="6"><i class="far fa-id-badge"></i> Mat.</th>
                        <th class="text-white all" data-priority="1"><i class="fas fa-signature"></i> Nome</th>
                        <th class="text-white desktop desktop-lg" data-priority="11"><i class="fab fa-black-tie"></i> Cat.</th>

                        <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="2"><i class="fas fa-plus"></i> Adic. Noturno</th>
                        <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="3"><i class="fas fa-calendar-day"></i> Feriados</th>
                        <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="4"><i class="far fa-calendar-times"></i> Faltas</th>
                        <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="5"><i class="far fa-clock"></i> Horas Extras</th>

                        <th class="bg-secondary no-sort desktop" data-priority="7"><i class="fas fa-plus"></i> Adic. Noturno</th>
                        <th class="bg-secondary no-sort desktop" data-priority="8"><i class="fas fa-calendar-day"></i> Feriados</th>
                        <th class="bg-secondary no-sort desktop" data-priority="9"><i class="far fa-calendar-times"></i> Faltas</th>
                        <th class="bg-secondary no-sort desktop" data-priority="10"><i class="far fa-clock"></i> Horas Extras</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($funcionarios as $funcionario)
                    @if($bloqueado || $funcionario->bloqueio_cbo || $funcionario->situacao == 'D' || $funcionario->situacao == 'T')
                        @php
                            $readonly = 'readonly'
                        @endphp
                    @else
                        @php
                            $readonly = '';
                        @endphp
                    @endif
                    @if($bloqueado || $funcionario->bloqueio_cbo || $funcionario->situacao == 'D' || $funcionario->situacao == 'T' || !\Auth::user()->can('eAdministrador', \App\User::class))
                    @php
                            $readonly_dp = 'readonly'
                        @endphp
                    @else
                        @php
                            $readonly_dp = '';
                        @endphp
                    @endif
                    <tr>
                        <td class="text-center">{{ str_pad($funcionario->matricula, 6, "0", STR_PAD_LEFT) }}</td>
                        <td class="text-smart text-left">
                            {{ $funcionario->nome }}
                            @if (in_array($funcionario->situacao, ['T', 'F', 'D']))
                                <span class="badge alert-danger">{{ $funcionario->situacao }}</span>
                            @endif
                        </td>
                        <td class="text-center">{{ $funcionario->catfunc }}</td>
                        <td>
                            <input type="text"
                                name="funcionario[{{$funcionario->matricula}}][horas_noturnas]"
                                id="horas_noturnas_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center time @error('funcionario.'.$funcionario->matricula.'.horas_noturnas') is-invalid @enderror"
                                maxlength="5"
                                value="{{ old('funcionario.'.$funcionario->matricula.'.horas_noturnas', $funcionario->horas_noturnas) }}"
                                {{ $readonly }}
                            >
                        </td>
                        <td>
                            <input type="text"
                                inputmode="numeric"
                                name="funcionario[{{$funcionario->matricula}}][feriados]"
                                id="feriados_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center  @error('funcionario.'.$funcionario->matricula.'.feriados') is-invalid @enderror"
                                maxlength="2"
                                onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57"
                                value="{{ old('funcionario.'.$funcionario->matricula.'.feriados', $funcionario->feriados) }}"
                                {{ $readonly }}
                            >
                        </td>
                        <td>
                            <input type="text"
                                inputmode="numeric"
                                name="funcionario[{{$funcionario->matricula}}][faltas]"
                                id="faltas_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center  @error('funcionario.'.$funcionario->matricula.'.faltas') is-invalid @enderror"
                                maxlength="2"
                                onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57"
                                value="{{ old('funcionario.'.$funcionario->matricula.'.faltas', $funcionario->faltas) }}"
                                {{ $readonly }}
                            >
                        </td>
                        <td>
                            <input type="text"
                                name="funcionario[{{$funcionario->matricula}}][horas_extras]"
                                id="horas_extras_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center time  @error('funcionario.'.$funcionario->matricula.'.horas_extras') is-invalid @enderror"
                                maxlength="5"
                                value="{{ old('funcionario.'.$funcionario->matricula.'.horas_extras', $funcionario->horas_extras) }}"
                                {{ $readonly }}
                            >
                        </td>
                        <td>
                            <input type="text"
                                name="funcionario[{{$funcionario->matricula}}][horas_noturnas_ma]"
                                id="horas_noturnas_ma_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center time  @error('funcionario.'.$funcionario->matricula.'.horas_noturnas_ma') is-invalid @enderror"
                                maxlength="5"
                                {{ $readonly_dp }}
                                value="{{ old('funcionario.'.$funcionario->matricula.'.horas_noturnas_ma', $funcionario->horas_noturnas_ma) }}"
                            >
                        </td>
                        <td>
                            <input type="text"
                                inputmode="numeric"
                                name="funcionario[{{$funcionario->matricula}}][feriados_ma]"
                                id="feriados_ma_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center  @error('funcionario.'.$funcionario->matricula.'.feriados_ma') is-invalid @enderror"
                                maxlength="2"
                                onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57"
                                {{ $readonly_dp }}
                                value="{{ old('funcionario.'.$funcionario->matricula.'.feriados_ma', $funcionario->feriados_ma) }}"
                            >
                        </td>
                        <td>
                            <input type="text"
                                inputmode="numeric"
                                name="funcionario[{{$funcionario->matricula}}][faltas_ma]"
                                id="faltas_ma_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center  @error('funcionario.'.$funcionario->matricula.'.faltas_ma') is-invalid @enderror"
                                maxlength="2"
                                onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57"
                                {{ $readonly_dp }}
                                value="{{ old('funcionario.'.$funcionario->matricula.'.faltas_ma', $funcionario->faltas_ma) }}"
                            >
                        </td>
                        <td>
                            <input type="text"
                                name="funcionario[{{$funcionario->matricula}}][horas_extras_ma]"
                                id="horas_extras_ma_{{ $funcionario->matricula }}"
                                class="form-control form-control-sm text-center time  @error('funcionario.'.$funcionario->matricula.'.horas_extras_ma') is-invalid @enderror"
                                maxlength="5"
                                {{ $readonly_dp }}
                                value="{{ old('funcionario.'.$funcionario->matricula.'.horas_extras_ma', $funcionario->horas_extras_ma) }}"
                            >
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="3" class="border text-info">Total de {{ $funcionarios->count() }} funcionários encontrados</th>
                        <th colspan="8" class="text-center border text-info">* Estes itens somente serão importados se o campo férias estiver em branco.</th>
                    </tr>
                </tfoot>
            </table>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <span>Legenda:</span>
                    <span class="badge alert-danger">F</span> Férias
                    <span class="badge alert-danger">T</span> Transferência
                    <span class="badge alert-danger">D</span> Demissão/Transferência
                </div>
            </div>
            @if(!$bloqueado)
            <div class="row mt-3 mb-2">
                <div class=" @cannot('eAdministrador', \App\User::class) col-md-6 @endcannot @can('eAdministrador', \App\User::class) col-md-3 @endcan">
                    <button type="button" class="btn btn-sm btn-success btn-block" onclick="createLoading('formLancamentos', 'Estamos tentando salvar os lançamentos.')" data-toggle="tooltip" title="Clique para salvar as alterações dos Lançamentos.">
                        <i class="far fa-save"></i> Salvar Alterações
                    </button>
                </div>
                <div class="@cannot('eAdministrador', \App\User::class) col-md-6 @endcannot @can('eAdministrador', \App\User::class) col-md-3 @endcan">
                    <button type="button" class="btn btn-sm btn-danger btn-block" onclick="location.reload();" data-toggle="tooltip" title="Clique aqui para cancelar os Lançamentos e recarregar a página.">
                        <i class="fas fa-times"></i> Cancelar
                    </button>
                </div>
                @can('eAdministrador', \App\User::class)
                    <div class="col-md-3">
                        <button type="button" class="btn btn-sm btn-info btn-block" onclick="exporta_ponto('xls')" data-toggle="tooltip" title="Clique aqui para exportar os Lançamentos em XLS (Legado).">
                            <i class="far fa-file-excel"></i> Exportar Tela em XLS
                        </button>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-sm btn-info btn-block" onclick="exporta_ponto('csv')" data-toggle="tooltip" title="Clique aqui para exportar os Lançamentos em CSV (Legado).">
                            <i class="fas fa-file-csv"></i> Exportar Tela em CSV
                        </button>
                    </div>
                @endcan
            </div>
            @endif
        </form>
    </div>
@endsection

@section('scripts')
<script>
    /**
    * Configurações de incialização da DataTable.
    */

    let positions =  "<'row' @can('eAdministrador', \App\User::class) <'col-sm-12 col-md-7'B> @endcan <'col-sm-12 @cannot('eAdministrador', \App\User::class) offset-md-7 @endcannot col-md-5'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mb-2'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

    let names = [
        'Matrícula',
        'Nome',
        'Categoria',
        'Adic. Noturno',
        'Feriados',
        'Faltas',
        'Horas Extras',
        'Adic. Noturno Mês Ant.',
        'Feridados Mês Ant.',
        'Faltas Mês Ant.',
        'Horas Extras Mês Ant.'
    ];

    let buttonCommon = {
        exportOptions: {
            columns: ':visible',
            format: {
                body: function (data, row, column, node) {
                    //check if type is input using jquery
                    try{
                        if($(data).is("input")){
                            return $(data).val();
                        }
                    }catch(e){}
                    let substr = data.indexOf('<');
                    if(substr !== -1){
                        data = data.replace('<span class="badge alert-danger">', '(');
                        data = data.replace('                                                           ', '');
                        data = data.replace(/\r\n/g, '').replace(/[\r\n]/g, '');
                        data = data.replace('</span>', ')');
                    }
                    return data;
                },
                header: function(data, colindex, trDOM, node){
                    return names[colindex];
                }
            }
        }
    };

    let options = {
        "dom": positions,
        "paging":   false,
        "ordering": true,
        "info":     false,
        "columnDefs": [
            {"orderable": false, "targets": 'no-sort'},
        ],
        "searching": true,
        "responsive": {
            breakpoints: [
                { name: 'desktop-xl',  width: Infinity },
                { name: 'desktop-lg',  width: 2559 },
                { name: 'desktop',  width: 1439 },
                { name: 'tablet-l', width: 1023 },
                { name: 'tablet-p', width: 767 },
                { name: 'mobile-l', width: 479 },
                { name: 'mobile-p', width: 319 }
            ]
        },
        "order": [[ 1, "asc" ]],
        @can('eAdministrador', \App\User::class)
        "buttons": [
            $.extend(true, {}, buttonCommon, {
                extend: 'print',
                text: '<i class="fas fa-print"></i>',
                title: 'Lançamentos - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para imprimir toda a tabela.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i>',
                title: 'Lançamentos - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para copiar toda a tabela.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i>',
                titleAttr: 'Clique aqui para exportar toda a tabela como CSV.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Clique aqui para exportar toda a tabela como XLS.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                title: 'Lançamentos - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como PDF.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'colvis',
                text: '<i class="fas fa-eye"></i>',
                titleAttr: 'Clique aqui para alterar a visualização de colunas.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                },
                columnText: function(dt, idx, title){
                    return names[idx];
                }
            }),
        ],
        @endcan
        "fixedHeader": {
            "header": true,
            "footer": true,
            headerOffset: $('#navbar_resumo_ponto').height()+15
        },
        "language": {
            "search": "<i class='fas fa-search'></i> Pesquisar:",
            "buttons": {
                copySuccess: {
                    1: "Copiada uma linha para área de transferência.",
                    _: "Copiadas %d linhas para a área de transferência."
                },
                copyTitle: "Copiado para a área de transferência.",

            }
        },
    };

    /**
    * Inicialização da DataTable.
    */

    let table = $('#tabela-lancamentos').DataTable(options);

    function resize(columns){
        let primeiras_colunas = [0, 1, 2];
        let colunas_mes_atual = [3, 4, 5, 6];
        let colunas_mes_anterior = [7, 8, 9, 10];

        let show_colunas_mes_aterior = false;
        colunas_mes_anterior.forEach((el, i, arr) => {
            if (columns[el] === true) show_colunas_mes_aterior = true;
        });
        if(show_colunas_mes_aterior){
            if($('#complex_header3').hasClass('hide')){
                $('#complex_header3').removeClass('hide');
            }
        }else{
            $('#complex_header3').addClass('hide');
        }

        let show_colunas_mes_atual = false;
        colunas_mes_atual.forEach((el, i, arr) => {
            if (columns[el] === true) show_colunas_mes_atual = true;
        });
        if(show_colunas_mes_atual){
            if($('#complex_header2').hasClass('hide')){
                $('#complex_header2').removeClass('hide');
            }
            if(columns[2] === false){
                $('#complex_header0').addClass('hide');
                if($('#complex_header1').hasClass('hide')){
                    $('#complex_header1').removeClass('hide');
                }
            }else{
                $('#complex_header1').addClass('hide');
                if($('#complex_header0').hasClass('hide')){
                    $('#complex_header0').removeClass('hide');
                }
            }
        }else{
            $('#complex_header2').addClass('hide');
        }

        if(columns[0] === false && columns[1] === true && columns[2] === false){
            if(!$('#complex_header0').hasClass('hide'))
                $('#complex_header0').addClass('hide');
            if(!$('#complex_header1').hasClass('hide'))
                $('#complex_header1').addClass('hide');
        }

        table.responsive.recalc();
    }

    /**
     * Ajuste de complex_headers para responsividade
    */
    table.on('responsive-resize', function(e, datatable, columns){
        resize(columns);
    });

    let responsive_hidden_columns = table.columns().responsiveHidden();
    resize(responsive_hidden_columns);

    function exporta_ponto(formato){
        let nome_arquivo = 'Resumo de Ponto ' + ("0" + new Date().getDate()).slice(-2) + '-' +("0" + (new Date().getMonth() + 1)).slice(-2) + '-' + new Date().getFullYear() + '.' + formato;
        exporta_loading('Extamos tentado exportar a tabela...', '/exporta/lancamentos/'+formato, nome_arquivo);
    }

    $('.time').mask('00:M0' , {
        translation: {
            'M': {
                pattern: /[0-5]/
            },
        }
    });

</script>
@endsection

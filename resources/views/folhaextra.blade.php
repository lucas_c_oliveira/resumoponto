@extends('layouts.app')

@section('page-header')
<div class="row">
    <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-2">
        <div class="form-group row mb-0">
            <label for="filial" class="col-lg-2 form-group col-form-label text-nowrap">
                <h4 class="mb-0 text-truncate"><i class="fas fa-print"></i> Impressão do Ponto</h4>
            </label>
            <div class="col-lg-3 form-group text-center">
                <div class="dropdown btn-block">
                    <button class="btn btn-info btn-sm btn-block dropdown-toggle" type="button" id="filial" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-second="tooltip" title="Clique para selecionar a Filial.">
                        <i class="fas fa-store-alt"></i> {{ $filiais['filial_escolhida']->nr_fil }} - {{ $filiais['filial_escolhida']->filial }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="filial" id="filial-label-dropdown-menu">
                        @foreach ($filiais['filiais'] as $filial)
                        <a class="dropdown-item @if($filiais['filial_escolhida']->nr_fil == $filial->nr_fil) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro para a Filial {{ $filial->nr_fil }} - {{ $filial->filial }}.', '{{URL::to('/filtrar_por_filial')}}?nr_fil={{ $filial->nr_fil }}')">
                            <i class="fas fa-store-alt"></i> {{ $filial->nr_fil }} - {{ $filial->filial }}
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-3 form-group text-center">
                <div class="dropdown btn-block">
                    <button class="btn btn-info btn-sm btn-block dropdown-toggle" type="button" id="setores" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-second="tooltip" title="Clique para selecionar o setor.">
                        <i class="fas fa-vector-square"></i> {{ $setores['setor_selecionado']['setor'] }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="setores" id="setores-label-dropdown-menu">
                        @foreach($setores['setores'] as $setor)
                            <a class="dropdown-item @if($setores['setor_selecionado']['setor_id'] == $setor->setor_id) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro de setor para {{ $setor->setor }}.', '{{URL::to('/filtrar_por_setor')}}?setor_id={{ $setor->setor_id }}')">
                                <i class="fas fa-vector-square"></i> {{ $setor->setor }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-2 form-group text-center">
                <div class="dropdown btn-block">
                    <button class="btn btn-info btn-sm btn-block dropdown-toggle text-truncate" type="button" id="periodos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-second="tooltip" title="Clique para selecionar o período.">
                        <i class="fas fa-calendar-alt"></i> {{ $periodos['periodo_selecionado']['mes_extenso'] }} de {{ $periodos['periodo_selecionado']['ano'] }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="periodos" id="periodos-label-dropdown-menu">
                        @foreach ($periodos['periodos'] as $periodo)
                        <a class="dropdown-item @if($periodos['periodo_selecionado']['ano_mes'] == $periodo->ano_mes ) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro para o período de {{ $periodo->mes_extenso }} de {{ $periodo->ano }}.', '{{URL::to('/filtrar_por_periodo')}}?ano_mes={{ $periodo->ano_mes }}')">
                            <i class="fas fa-calendar-alt"></i> {{ $periodo->mes_extenso }} de {{ $periodo->ano }}
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-2 form-group text-center">
                <button type="button" class="btn btn-outline-info btn-sm btn-block btn-block text-truncate" id="btn_folha_geral" onclick="folhaGeral()" data-toggle="tooltip" title="Gerar todas as folhas de Ponto.">
                    <div class="btn-content i-normal mostra text-truncate">
                        <i class="fas fa-download"></i> Gerar Folhas de Ponto
                    </div>
                    <div class="btn-content i-loader text-truncate">
                        <div class="spinner-border spinner-border-sm text-info2" role="status">
                            <span class="sr-only">Carregando...</span>
                        </div>
                        Carregando...
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
    <table class="table table-striped table-hover table-sm" width="100%" id="table-folha">
        <thead class="bg-info text-white text-center">
            <tr>
                <th><i class="far fa-id-badge"></i> Matrícula</th>
                <th><i class="fas fa-signature"></i> Nome</th>
                <th><i class="fab fa-black-tie"></i> Cat.</th>
                <th><i class="fas fa-vector-square"></i> Setor</th>
                <th><i class="fas fa-briefcase"></i> Cargo</th>
                <th><i class="fas fa-download"></i> Baixar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($funcionarios as $func)
                <tr>
                    <td class="text-center">{{ str_pad($func->matricula, 6, "0", STR_PAD_LEFT) }}</td>
                    <td nowrap class="lf">
                        {{ $func->nome }}
                        @if (in_array($func->situacao, ['T', 'F', 'D']))
                        <span class="badge alert-danger">{{ $func->situacao }}</span>
                        @endif
                    </td>
                    <td class="text-center">{{ $func->catfunc }}</td>
                    <td class="text-center">{{ $func->setor }}</td>
                    <td class="text-center">{{ $func->cargo }}</td>
                    <td class="text-center" width="13%">
                        <button onclick="folhaUnica('{{ $func->nr_fil }}','{{ $func->matricula }}','{{ $func->cod_cbo }}','{{ $func->cargo }}','{{ $func->setor }}')"
                            type="button" id="btn_folha_unica_{{$func->matricula}}"
                            class="btn btn-outline-info btn-sm btn-block text-truncate"
                            data-toggle="tooltip" title="Clique para gerar a folha de ponto de {{ $func->nome }}">
                            <div class="btn-content i-normal mostra text-truncate">
                                <i class="fas fa-download"></i> Folha
                            </div>
                            <div class="btn-content i-loader text-truncate">
                                <div class="spinner-border spinner-border-sm text-info2" role="status">
                                    <span class="sr-only">Carregando...</span>
                                </div>
                                Carregando...
                            </div>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-12 mt-1 mb-3">
            <span>Legenda:</span>
            <span class="badge alert-danger">F</span> Férias
            <span class="badge alert-danger">T</span> Transferência
            <span class="badge alert-danger">D</span> Demissão/Transferência
        </div>
    </div>
@endsection

@php
    $tam = count($diasMesPonto);
    $tamfunc = count($funcionarios);
    function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
@endphp

@section('scripts')
<script>
    /**
     * Configurações de incialização da DataTable.
    */

    function initializeOptions(breakpoint){
        if(breakpoint == 'xSmall' || breakpoint == 'small'){
            return "full";
        }else if(breakpoint == 'medium'){
            return "simple_numbers";
        }else if(breakpoint == 'large' || breakpoint == 'xLarge'){
            return "full_numbers";
        }
    }

    let positions = "<'row mb-2 mt-2'<'col-sm-12 col-md-12 text-center'B>>" +
        "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mb-2'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

    let options = {
        "dom": positions,
        "paging":   true,
        "paginationType": initializeOptions(bsBreakpoints.getCurrentBreakpoint()),
        "ordering": true,
        "info":     true,
        "columnDefs": [
            {"orderable": false, "targets": 'no-sort'},
        ],
        "searching": true,
        "responsive": {
            breakpoints: [
                { name: 'desktop-xl',  width: Infinity },
                { name: 'desktop-lg',  width: 2559 },
                { name: 'desktop',  width: 1439 },
                { name: 'tablet-l', width: 1023 },
                { name: 'tablet-p', width: 767 },
                { name: 'mobile-l', width: 479 },
                { name: 'mobile-p', width: 319 }
            ]
        },
        "lengthMenu": [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "Todos"]],
        "responsive": true,
        "buttons": [
            {
                extend: 'print',
                text: '<i class="fas fa-print"></i> Imprimir',
                title: 'Usuários - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para imprimir toda a tabela.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i> Copiar',
                titleAttr: 'Clique aqui para copiar toda a tabela.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i> Exportar como CSV',
                titleAttr: 'Clique aqui para exportar toda a tabela como CSV.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i> Exportar como Excel',
                titleAttr: 'Clique aqui para exportar toda a tabela como XLS.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i> Exportar como PDF',
                title: 'Usuários - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como PDF.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fas fa-eye"></i> Vizualizar Colunas',
                titleAttr: 'Clique aqui para alterar a visualização de colunas.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
        ],
        "fixedHeader": {
            "header": true,
            headerOffset: $('#navbar_resumo_ponto').height()+15
        },
        "language": {
            "search": "<i class='fas fa-search'></i> Pesquisar:",
            "zeroRecords": "Usuário não encontrado!",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first":      "<i class=\"fas fa-angle-double-left\"></i>",
                "last":       "<i class=\"fas fa-angle-double-right\"></i>",
                "next":       "<i class=\"fas fa-angle-right\"></i>",
                "previous":   "<i class=\"fas fa-angle-left\"></i>"
            },
            "info": "Mostrando _START_ até _END_ de _TOTAL_ usuários.",
            "infoFiltered": " Filtrado de _MAX_ usuários.",
            "emptyTable": "Nenhum usuaário na tabela.",
            "zeroRecords": "Nenhum usuário encontrado.",
            "infoEmpty": "Nenhum usuário a ser mostrado.",
            "buttons": {
                copySuccess: {
                    1: "Copiada uma linha para área de transferência.",
                    _: "Copiadas %d linhas para a área de transferência."
                },
                copyTitle: "Copiado para a área de transferência.",

            }
        },
        "order": [],
        "drawCallback": function(){
            paginationButtons();
        }
    };

    /**
     * Inicialização da DataTable.
    */

    let table = $('#table-folha').DataTable(options);

    /**
     * Altração da paginação quando necessário.
    */
    function alteraOptions(breakpoint){
        if(breakpoint == 'xSmall' || breakpoint == 'small'){
            options.paginationType = "full";
            table.destroy();
            table = $('#table-folha').DataTable(options);
        }else if(breakpoint == 'medium'){
            options.paginationType = "simple_numbers";
            table.destroy();
            table = $('#table-folha').DataTable(options);
        }else if(breakpoint == 'large' || breakpoint == 'xLarge'){
            options.paginationType = "full_numbers";
            table.destroy();
            table = $('#table-folha').DataTable(options);
        }
    }
    $(window).on('new.bs.breakpoint', function(e){
        alteraOptions(e.breakpoint);
    });

    function paginationButtons(){
       $('a[data-dt-idx]').addClass('btn btn-sm btn-info border-0 text rounded-0');
       $('a.first').removeClass('rounded-0');
       $('a.last').removeClass('rounded-0');
       $('a.first').addClass('rounded-left');
       $('a.last').addClass('rounded-right');
    }

    $('#table-folha tbody').on('mouseover', 'tr', function () {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover',
        });
    });

    /*
    * Funções de Exportação das Folhas de ponto.
    * Declaração de variáveis globais.
    */

    var inserido = false;
    var inseridoTelevendas = false;

    /*
    * Funções de insert.
    */

    function insertDias(){
        if(!inserido){
            var tableDates = $('#diasToInsert').html();
            var alvo = $('.alvoDias');
            $(tableDates).appendTo(alvo);
            inserido = true;
        }
    }

    function insertDiasTelevendas(){
        if(!inseridoTelevendas){
            var tableDatas = $('#diasToInsert').html();
            var alvo = $('.alvoDiasTelevendas');
            $(tableDatas).appendTo(alvo);
            inseridoTelevendas = true;
        }
    }

    function generateDocTelevendas(matricula){
        insertDiasTelevendas();
        generateDocTelevendas(matricula);
    }

    function generateDocNormal(matricula){
        insertDias();
        generateDoc(matricula);
    }

    function generateDoc(matricula){
        var doc = new jsPDF();
        doc.autoTable({
            html: '#table-ponto-' + matricula,
            useCss: 'true',
            margin: { left: 15, top: 10, right: 10 }
        });
        doc.save('Folha_Ponto_Matricula_'+matricula+'.pdf');
    }

    function generateDocTelevendas(matricula){
        var doc = new jsPDF();
        doc.autoTable({
            html: '#table-ponto-televendas-' + matricula,
            useCss: 'true',
            margin: { left: 15, top: 10, right: 10 }
        });
        doc.save('Folha_Ponto_Matricula_'+matricula+'.pdf');
    }

    function generateDocCompleta(matricula, doc){
        doc.autoTable({
            html: '#table-ponto-' + matricula,
            useCss: 'true',
            margin: { left: 15, top: 10, right: 10 }
        });
    }

    function generateDocTelevendasCompleta(matricula, doc){
        doc.autoTable({
            html: '#table-ponto-' + matricula,
            useCss: 'true',
            margin: { left: 15, top: 10, right: 10 }
        });
    }

    function folhaUnica(filial, matricula, cod_cbo, cargo, setor){
        $('#btn_folha_unica_'+matricula+' .i-normal').fadeOut(function(){
            $('#btn_folha_unica_'+matricula+' .i-normal').toggleClass('mostra');
            $('#btn_folha_unica_'+matricula+' .i-loader').fadeIn(function(){
                if(filial == '007' && cod_cbo == '422305'){
                    generateDocNormal(matricula);
                }else if(filial == '007' && cod_cbo.search('442') == 0 && cargo.search('ATEND') == 0){
                    generateDocTelevendas(matricula);
                }else if(filial == '903' && (cargo.search('ATEND') == 0 || cargo.search('ORCA') == 0)){
                    generateDocTelevendas(matricula);
                }else if(cod_cbo.search('A0341') == 0){
                    generateDocTelevendas(matricula);
                }else if(filial == '007' && setor.search('LAB') == 0){
                    generateDocTelevendas(matricula);
                }else if(filial == '002' && setor.search('ORCA') == 0){
                    generateDocTelevendas(matricula);
                }else if(filial == '900' && setor.search('SAC') == 0){
                    generateDocTelevendas(matricula);
                }else{
                    generateDocNormal(matricula);
                }
                $('#btn_folha_unica_'+matricula+' .i-loader').fadeOut(function(){
                    $('#btn_folha_unica_'+matricula+' .i-normal').toggleClass('mostra');
                });
            });
        });
    }

    function folhaGeral(){
        $('#btn_folha_geral .i-normal').fadeOut(function(){
            $('#btn_folha_geral .i-normal').toggleClass('mostra');
            $('#btn_folha_geral .i-loader').fadeIn(function(){
                geraPontoEncapsulado();
                $('#btn_folha_geral .i-loader').fadeOut(function(){
                    $('#btn_folha_geral .i-normal').toggleClass('mostra');
                });
            });
        });
    }

    function geraPontoEncapsulado(callback){
        insertDiasTelevendas();
        insertDias();
        var doc = new jsPDF();
        var filial;
        var cod_cbo;
        var cargo;
        var setor;
        var matricula;
        @foreach ($funcionarios as $key => $func)
            filial = '{{ $func->nr_fil }}'
            cod_cbo = '{{ $func->cod_cbo }}'
            cargo = '{{ $func->cargo }}'
            setor = '{{ $func->setor }}'
            matricula = '{{ $func->matricula }}'
            if(filial == '007' && cod_cbo == '422305'){
                generateDocCompleta(matricula, doc);
            }else if(filial == '007' && cod_cbo.search('442') == 0 && cargo.search('ATEND') == 0){
                generateDocTelevendasCompleta(matricula, doc);
            }else if(filial == '903' && (cargo.search('ATEND') == 0 || cargo.search('ORCA') == 0)){
                generateDocTelevendasCompleta(matricula, doc);
            }else if(cod_cbo.search('A0341') == 0){
                generateDocTelevendasCompleta(matricula, doc);
            }else if(filial == '007' && setor.search('LAB') == 0){
                generateDocTelevendasCompleta(matricula, doc);
            }else if(filial == '002' && setor.search('ORCA') == 0){
                generateDocTelevendasCompleta(matricula, doc);
            }else if(filial == '900' && setor.search('SAC') == 0){
                generateDocTelevendasCompleta(matricula, doc);
            }else{
                generateDocCompleta(matricula, doc);
            }
            @if($key != $tamfunc-1)
            doc.addPage();
            @endif
        @endforeach
        var filial_escolhida = '{{$filiais['filial_escolhida']->nr_fil}}';
        doc.save('Folhas_de_Ponto_Filial_'+filial_escolhida+'.pdf');
    }
</script>
@endsection

@section('tabela_ponto')
{{-- <div class="wrapper-folha-ponto"> --}}
@foreach($funcionarios as $func_ponto)
{{-- tabela ponto para impressão--}}
<table id="table-ponto-{{ $func_ponto->matricula }}" class="table-folha-ponto">

    <tbody class="table-header">
        <tr>
            <td colspan="11" class="cell-cente b">
                FOLHA DE PONTO PERÍODO: {{ $diasMesPonto[0]['data']}} a {{ $diasMesPonto[$tam-1]['data']}}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-left b">
                Empresa: IRMÃOS MATTAR & CIA LTDA - CNPJ: {{ mask($func_ponto->l_cnpj,'##.###.###/####-##') }} &nbsp;&nbsp; Loja: {{ $func_ponto->nr_fil }} - {{ $func_ponto->l_nome }}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-left b">
                End.:

                {{ $func_ponto->l_endereco }} - {{ $func_ponto->l_bairro }} -
                {{ $func_ponto->l_cidade }} / {{ $func_ponto->l_uf }}
            </td>
        </tr>
        <tr>
            <td colspan="9" class="cell-left b">
                Nome: {{ $func_ponto->nome }}
            </td>
            <td class="cell-left b" colspan="2">
                Matrícula: {{ $func_ponto->matricula }}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-left b">
                Cargo: {{ strtoupper($func_ponto->cargo) }}
            </td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td colspan="4" class="b">HORAS NORMAIS</td>
            <td colspan="4" class="b">HORAS EXTRAS</td>
            <td></td>
        </tr>


        <tr class="b">
            <td>Dia</td>
            <td>Dia</td>
            <td colspan="2" class="cell-center">1ª Jornada</td>
            <td colspan="2" class="cell-center">2ª Jornada</td>

            <td colspan="2" class="cell-center">1ª Jornada</td>
            <td colspan="2" class="cell-center">2ª Jornada</td>
            <td>Assinatura do</td>
        </tr>
        <tr class="b">
            <td class="cell-time">Mês</td>
            <td class="cell-time">Sem.</td>
            <td class="cell-timed">Entrada</td>
            <td class="cell-timex">Saida</td>
            <td class="cell-timed">Entrada</td>
            <td class="cell-timex">Saída</td>
            <td class="cell-timed">Entrada</td>
            <td class="cell-timex">Saída</td>
            <td class="cell-timed">Entrada</td>
            <td class="cell-timex">Saída</td>
            <td class="cell-sign">Funcionário</td>
        </tr>
    </tbody>
    <tbody class="table-header alvoDias">

        {{-- insert dias --}}

    </tbody>

    <tbody class="table-header">

        <tr>
            <td colspan="11" class="cell-legenda nome-foot b">
                Declaro que os horários acima foram anotados por mim e estão corretos.
            </td>
        </tr>
        <tr>
            <td colspan="11">

                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="11">
                {{ $func_ponto->nome }}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-center b NOVOX">
                <i>Atenção!!! Não é permitido rasuras e o uso de corretivo "error-ex" nas folhas de ponto</i>
            </td>
        </tr>
    </tbody>
</table>
{{-- fecha tabela ponto --}}
@endforeach
{{-- </div> --}}

{{-- <div class="wrapper-folha-ponto"> --}}
@foreach($funcionarios as $func_ponto)
{{-- tabela ponto televendas para impressão--}}
<table id="table-ponto-televendas-{{ $func_ponto->matricula }}" class="table-folha-ponto" >
    <tbody class="table-header">
        <tr>
            <td colspan="11" class="cell-cente b">
                FOLHA DE PONTO PERÍODO: {{ $diasMesPonto[0]['data']}} a {{ $diasMesPonto[$tam-1]['data']}}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-left b">
                Empresa: IRMÃOS MATTAR & CIA LTDA - CNPJ: {{ mask($func_ponto->l_cnpj,'##.###.###/####-##') }} &nbsp;&nbsp; Loja: {{ $func_ponto->nr_fil }} - {{ $func_ponto->l_nome }}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-left b">
                End.:
                {{ strtoupper($func_ponto->l_endereco) }} - {{ strtoupper($func_ponto->l_bairro) }} -
                {{ strtoupper($func_ponto->l_cidade) }} / {{ $func_ponto->l_uf }}
            </td>
        </tr>
        <tr>
            <td colspan="9" class="cell-left b">
                Nome: {{ $func_ponto->nome }}
            </td>
            <td class="cell-left b" colspan="2">
                Matrícula: {{ $func_ponto->matricula }}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-left b">
                Cargo: {{ strtoupper($func_ponto->cargo) }}
            </td>
        </tr>
        <tr>
            <td colspan="11" class="b title-tele">
                HORÁRIO TRABALHADO - TELE ATENDIMENTO
            </td>
        </tr>
        <tr>
            <td colspan="3" class="info">A primeira pausa deve ser posterior a 60 minutos trabalhados.</td>
            <td colspan="2" class="b-tele-info">1ª Pausa Obrigatória 10min</td>
            <td colspan="2" class="b-tele-info">Intervalo de descanso 20min</td>
            <td colspan="2" class="b-tele-info">2ª Pausa Obrigatória 10min</td>
            <td colspan="2" class="info">A segunda pausa deve ser concedida antes dos últmos 60 minutos de traballho de acordo com o anexo II da NR 17.</td>
        </tr>
        <tr class="b">
            <td class="b-tele-dias">Dia do <br>Mês</td>
            <td class="b-tele-dias">Dia da <br>Semana</td>
            <td class="b-tele">Entrada</td>
            <td class="b-tele">Intervalo</td>
            <td class="b-tele">Retorno</td>
            <td class="b-tele">Intervalo</td>
            <td class="b-tele">Retorno</td>
            <td class="b-tele">Intervalo</td>
            <td class="b-tele">Retorno</td>
            <td class="b-tele-saida">Saída</td>
            <td class="cell-sign-tele">Assinatura</td>
        </tr>
    </tbody>
    <tbody class="table-header alvoDiasTelevendas">
        {{-- insert dias --}}
    </tbody>
    <tbody class="table-header">
        <tr>
            <td colspan="11" class="cell-legenda nome-foot b">
                Declaro que os horários acima foram anotados por mim e estão corretos.
            </td>
        </tr>
        <tr>
            <td colspan="11">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="11">
                {{ $func_ponto->nome }}

            </td>
        </tr>
        <tr>
            <td colspan="11" class="cell-center b NOVOX">
                <i>Atenção!!! Não é permitido rasuras e o uso de corretivo "error-ex" nas folhas de ponto</i>
            </td>
        </tr>
    </tbody>
</table>

{{-- fecha tabela ponto televendas --}}
@endforeach
{{-- </div> --}}

<table class="table table-insert">
<tbody id="diasToInsert">
    @foreach ($diasMesPonto as $dia)

    {{-- dias mês --}}
    <tr @if ($dia['feriado']) class="cell-folga" @endif>

        <td class="cell-data">{{ $dia['data'] }}</td>
        <td class="cell-dia cell-left">{{ $dia['dayOfWeek'] }}</td>

        <td></td>
        <td></td>

        @if ($dia['dayOfWeek']=='Sábado')
        <td class="cell-folga"></td>
        <td class="cell-folga"></td>
        @else
        <td></td>
        <td></td>
        @endif

        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>

    </tr>

    @endforeach
</tbody>
</table>

@endsection

@if (count($errors->all()) > 0)
<div class="alert alert-danger alert-dismissable mt-2">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	@if (count($errors->all()) == 1) <h4>Erro encontrado</h4>
    @else <h4>Erros encontrados</h4> @endif
    @foreach ( $errors->all() as $m )
    <li>{!! $m !!}</li>
    @endforeach
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block mt-2">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	@if ( is_array($message) )
          @foreach ( $message as $m )
            <li>{!! $m !!}</li>
          @endforeach
        @else
          {!! $message !!}
        @endif
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block mt-2">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	@if ( is_array($message) )
          @foreach ( $message as $m )
            <li>{!! $m !!}</li>
          @endforeach
        @else
          {!! $message !!}
        @endif
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block mt-2">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	@if ( is_array($message) )
          @foreach ( $message as $m )
            <li>{!! $m !!}</li>
          @endforeach
        @else
          {!! $message !!}
        @endif
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-block mt-2">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	@if ( is_array($message) )
          @foreach ( $message as $m )
            <li>{!! $m !!}</li>
          @endforeach
        @else
          {!! $message !!}
        @endif
</div>
@endif

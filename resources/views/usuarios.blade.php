@extends('layouts.app')

@section('page-header')
    <form method="POST" action="{{URL::to('/usuarios')}}" id="user_create">
        @csrf
        @method('POST')
        <div class="row w-100 mt-1 pt-2 mb-1 pb-1 border border-rounded bg-secondary">

            <div class="col-md-12 mb-2">
                <h3><i class="fas fa-user-plus"></i> Adicionar Usuário</h3>
            </div>

            <div class="col-md-5 form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text border-0 bg-white">
                            <i class="far fa-id-badge"></i>
                        </span>
                    </div>
                    <input type="text"
                        name="matricula"
                        id="matricula"
                        class="form-control input-sm matricula border-0 shadow-none @error('matricula') is-invalid @enderror"
                        placeholder="Matrícula"
                        inputmode="numeric"
                        maxlength="6"
                        data-toggle="tooltip"
                        title="Entre com a Matrícula do Usuário no Protheus"
                        onkeypress="return event.keyCode === 8 || event.charCode >= 48 && event.charCode <= 57"
                        autocomplete="off">
                    @error('matricula')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-5 form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text border-0 bg-white">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                    <input type="text"
                        name="usuario"
                        id="usuario"
                        class="form-control form-sm usuario border-0 shadow-none @error('usuario') is-invalid @enderror"
                        placeholder="Usuário"
                        data-toggle="tooltip"
                        title="Entre com o Usuário no Active Directory"
                        autocomplete="off"
                        maxlength="100">
                    @error('usuario')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-2 form-group">
                <button type="button" class="btn btn-sm btn-outline-info btn-block text-truncate form-control"
                    data-toggle="tooltip" title="Clique neste botão para adicionar um novo Usuário."
                    onclick="createLoading('user_create', 'Estamos tentando crirar o usuário '+ $('#usuario').val() +'.')">
                    <i class="fas fa-user-plus"></i> Adicionar
                </button>
            </div>
        </div>
    </form>
@endsection

@section('content')
    <table class="table display dt-responsive table-sm" id="table-usuarios" style="width: 100%">
        <thead class="bg-info text-white">
            <tr>
                <th class="text-nowrap mobile-l tablet-p tablet-l desktop desktop-lg"><i class="far fa-id-badge"></i> Matrícula</th>
                <th class="text-nowrap tablet-p tablet-l desktop desktop-lg"><i class="fas fa-user"></i> Usuário</th>
                <th class="text-nowrap all"><i class="fas fa-signature"></i> Nome</th>
                <th class="text-nowrap tablet-l desktop desktop-lg"><i class="fas fa-briefcase"></i> Cargo</th>
                <th class="text-nowrap tablet-l desktop desktop-lg"><i class="fas fa-vector-square"></i> Setor</th>
                <th class="text-nowrap tablet-l desktop desktop-lg no-sort"><i class="fas fa-user-minus"></i> Excluir</th>
            </tr>
        </thead>
        <tbody>
        @foreach($usuarios as $usuario)
        <tr>
            <td>{{ str_pad($usuario->matricula, 6, "0", STR_PAD_LEFT) }}</td>
            <td>{{ $usuario->usuario }}</td>
            <td>{{ $usuario->nome }}</td>
            <td>{{ $usuario->cargo }}</td>
            <td>{{ $usuario->setor }}</td>
            <td>
                <form method="POST" action="{{URL::to('/usuarios')}}/{{ $usuario->id }}" class="form-inline" id="delete_{{$usuario->id}}">
                    @csrf
                    @method('DELETE')
                    <div>
                        <button type="button"
                            class="btn btn-outline-danger btn-sm text-nowrap"
                            data-toggle="tooltip"
                            title="Clique aqui para excluir o registro de {{ $usuario->nome }}"
                            onclick="deleteConfirm('Tem certeza de que deseja excluir o registro de {{ $usuario->nome }}?', 'delete_{{$usuario->id}}', 'Estamos tentando excluir o registro de {{$usuario->nome}}.', 'user-times')">
                            <i class="fas fa-user-times"></i> Excluir
                        </button>
                    </div>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
<script>
    /**
     * Configurações de incialização da DataTable.
    */

    function initializeOptions(breakpoint){
        if(breakpoint == 'xSmall' || breakpoint == 'small'){
            return "full";
        }else if(breakpoint == 'medium'){
            return "simple_numbers";
        }else if(breakpoint == 'large' || breakpoint == 'xLarge'){
            return "full_numbers";
        }
    }

    let positions = "<'row mb-2 mt-2'<'col-sm-12 col-md-12 text-center'B>>" +
        "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mb-2'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

    let options = {
        "dom": positions,
        "paging":   true,
        "paginationType": initializeOptions(bsBreakpoints.getCurrentBreakpoint()),
        "ordering": true,
        "info":     true,
        "columnDefs": [
            {"orderable": false, "targets": 'no-sort'},
        ],
        "searching": true,
        "responsive": {
            breakpoints: [
                { name: 'desktop-xl',  width: Infinity },
                { name: 'desktop-lg',  width: 2559 },
                { name: 'desktop',  width: 1439 },
                { name: 'tablet-l', width: 1023 },
                { name: 'tablet-p', width: 767 },
                { name: 'mobile-l', width: 479 },
                { name: 'mobile-p', width: 319 }
            ]
        },
        "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "Todos"]],
        "responsive": true,
        "buttons": [
            {
                extend: 'print',
                text: '<i class="fas fa-print"></i> Imprimir',
                title: 'Usuários - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para imprimir toda a tabela.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i> Copiar',
                titleAttr: 'Clique aqui para copiar toda a tabela.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i> Exportar como CSV',
                titleAttr: 'Clique aqui para exportar toda a tabela como CSV.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i> Exportar como Excel',
                titleAttr: 'Clique aqui para exportar toda a tabela como XLS.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i> Exportar como PDF',
                title: 'Usuários - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como PDF.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fas fa-eye"></i> Vizualizar Colunas',
                titleAttr: 'Clique aqui para alterar a visualização de colunas.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
        ],
        "fixedHeader": {
            "header": true,
            headerOffset: $('#navbar_resumo_ponto').height()+15
        },
        "language": {
            "search": "<i class='fas fa-search'></i> Pesquisar:",
            "zeroRecords": "Usuário não encontrado!",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first":      "<i class=\"fas fa-angle-double-left\"></i>",
                "last":       "<i class=\"fas fa-angle-double-right\"></i>",
                "next":       "<i class=\"fas fa-angle-right\"></i>",
                "previous":   "<i class=\"fas fa-angle-left\"></i>"
            },
            "info": "Mostrando _START_ até _END_ de _TOTAL_ usuários.",
            "infoFiltered": " Filtrado de _MAX_ usuários.",
            "emptyTable": "Nenhum usuaário na tabela.",
            "zeroRecords": "Nenhum usuário encontrado.",
            "infoEmpty": "Nenhum usuário a ser mostrado.",
            "buttons": {
                copySuccess: {
                    1: "Copiada uma linha para área de transferência.",
                    _: "Copiadas %d linhas para a área de transferência."
                },
                copyTitle: "Copiado para a área de transferência.",

            }
        },
        "drawCallback": function(){
            paginationButtons();
        }
    };

    /**
     * Inicialização da DataTable.
    */

    let table = $('#table-usuarios').DataTable(options);

    /**
     * Altração da paginação quando necessário.
    */
    function alteraOptions(breakpoint){
        if(breakpoint == 'xSmall' || breakpoint == 'small'){
            options.paginationType = "full";
            table.destroy();
            table = $('#table-usuarios').DataTable(options);
        }else if(breakpoint == 'medium'){
            options.paginationType = "simple_numbers";
            table.destroy();
            table = $('#table-usuarios').DataTable(options);
        }else if(breakpoint == 'large' || breakpoint == 'xLarge'){
            options.paginationType = "full_numbers";
            table.destroy();
            table = $('#table-usuarios').DataTable(options);
        }
    }
    $(window).on('new.bs.breakpoint', function(e){
        alteraOptions(e.breakpoint);
    });

    function paginationButtons(){
       $('a[data-dt-idx]').addClass('btn btn-sm btn-info border-0 text rounded-0');
       $('a.first').removeClass('rounded-0');
       $('a.last').removeClass('rounded-0');
       $('a.first').addClass('rounded-left');
       $('a.last').addClass('rounded-right');
    }

    $('#table-usuarios tbody').on('mouseover', 'tr', function () {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover',
        });
    });
</script>
@endsection

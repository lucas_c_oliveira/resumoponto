@extends('layouts.app')

@section('page-header')
<div class="row">
    <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-1">
        <div class="form-group row mb-0">
            <label for="filial" class="col-lg-4 form-group col-form-label">
                <h4 class="mb-0"><i class="fas fa-check"></i> Totais</h4>
            </label>
            <div class="offset-lg-4 col-lg-4 form-group text-center">
                <div class="dropdown btn-block">
                    <button class="btn btn-info btn-sm btn-block dropdown-toggle" type="button" id="periodos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-second="tooltip" title="Escolha o período.">
                        <i class="fas fa-calendar-alt"></i> {{ $periodos['periodo_selecionado']['mes_extenso'] }} de {{ $periodos['periodo_selecionado']['ano'] }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="periodos" id="periodos-label-dropdown-menu">
                        @foreach ($periodos['periodos'] as $periodo)
                        <a class="dropdown-item @if($periodos['periodo_selecionado']['ano_mes'] == $periodo->ano_mes ) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro para o período de {{ $periodo->mes_extenso }} de {{ $periodo->ano }}.', '{{URL::to('/filtrar_por_periodo')}}?ano_mes={{ $periodo->ano_mes }}')">
                            <i class="fas fa-calendar-alt"></i> {{ $periodo->mes_extenso }} de {{ $periodo->ano }}
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-1 bg-white">
        <div class="row">
            <div class="col-lg-3 text-center form-group"><b>Legenda:</b></div>
            <div class="col-lg-3 bg-primary text-center text-white form-group pl-3 pr-3"><b><i class="far fa-calendar"></i> Mês atual ({{ $periodos['periodo_selecionado']['mes_extenso'] }})</b></div>
            <div class="offset-lg-1 col-lg-3 bg-secondary text-center form-group pl-3 pr-3"><b><i class="far fa-calendar"></i> Mês anterior ({{ $periodos['periodo_passado']['mes_extenso'] }})</b></div>
        </div>
    </div>
</div>
@endsection

@section('content')
<table class="table table-sm table-hover table-striped display dt-responsive" id="tabela-totais" width="100%"  cellspacing="0">
    <thead>
        {{-- <tr class="text-center">
            <th colspan="2" class="border-0" id="complex_header1"></th>
            <th colspan="4" class="bg-primary border-bottom text-white" id="complex_header2"><i class="far fa-calendar"></i> Mês atual ({{ $periodos['periodo_selecionado']['mes_extenso'] }})</th>
            <th colspan="4" class="bg-light border-bottom" id="complex_header3"><i class="far fa-calendar"></i> Mês anterior ({{ $periodos['periodo_passado']['mes_extenso'] }})</th>
        </tr> --}}
        <tr class="text-center bg-info">
            <th class="text-white tablet-p tablet-l desktop desktop-lg" data-priority="6"><i class="fas fa-layer-group"></i> Região.</th>
            <th class="text-white all" data-priority="1"><i class="far fa-building"></i> Filial</th>

            <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="2"><i class="fas fa-plus"></i> Adic. Noturno</th>
            <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="3"><i class="fas fa-calendar-day"></i> Feriados</th>
            <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="4"><i class="far fa-calendar-times"></i> Faltas</th>
            <th class="bg-primary text-white no-sort tablet-l desktop" data-priority="5"><i class="far fa-clock"></i> Horas Extras</th>

            <th class="bg-secondary no-sort tablet-l desktop" data-priority="7"><i class="fas fa-plus"></i> Adic. Noturno</th>
            <th class="bg-secondary no-sort tablet-l desktop" data-priority="8"><i class="fas fa-calendar-day"></i> Feriados</th>
            <th class="bg-secondary no-sort tablet-l desktop" data-priority="9"><i class="far fa-calendar-times"></i> Faltas</th>
            <th class="bg-secondary no-sort tablet-l desktop" data-priority="10"><i class="far fa-clock"></i> Horas Extras</th>
        </tr>
    </thead>
    <tbody>
        @foreach($totais as $total)
        <tr>
            <td nowrap class="text-center">{{ $total->regiao }}</td>
            <td nowrap class="lf text-smart">
                {{ str_pad($total->nr_fil, 3, "0", STR_PAD_LEFT) }} {{ $total->filial }}
            </td>

            <td>{{ $total->horas_noturnas }}</td>
            <td>{{ $total->feriados }}</td>
            <td>{{ $total->faltas }}</td>
            <td>{{ $total->horas_extras }}</td>
            {{-- <td>{{ $total->domingos }}</td> --}}


            <td>{{ $total->horas_noturnas_ma }}</td>
            <td>{{ $total->feriados_ma }}</td>
            <td>{{ $total->faltas_ma }}</td>
            <td>{{ $total->horas_extras_ma }}</td>
            {{-- <td class="bg2">{{ $total->domingos_ma }}</td> --}}
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        @php
            $dias_ferias        = 0.00;
            $dias_atestado      = 0.00;
            $horas_noturnas     = 0.00;
            $feriados           = 0.00;
            $faltas             = 0.00;
            $horas_extras       = 0.00;
            $domingos           = 0.00;
            $horas_noturnas_ma  = 0.00;
            $feriados_ma        = 0.00;
            $faltas_ma          = 0.00;
            $horas_extras_ma    = 0.00;
            $domingos_ma        = 0.00;

            foreach($totais as $total)
            {
                $dias_ferias        += $total->dias_ferias;
                $dias_atestado      += $total->dias_atestado;
                $horas_noturnas     += $total->horas_noturnas;
                $feriados           += $total->feriados;
                $faltas             += $total->faltas;
                $horas_extras       += $total->horas_extras;
                $domingos           += $total->domingos;
                $horas_noturnas_ma  += $total->horas_noturnas_ma;
                $feriados_ma        += $total->feriados_ma;
                $faltas_ma          += $total->faltas_ma;
                $horas_extras_ma    += $total->horas_extras_ma;
                $domingos_ma        += $total->domingos_ma;
            }
        @endphp
        <th colspan="2" class="text-center"><b>Totais:</b></th>

        <th class="bg1">{{ $horas_noturnas }}</th>
        <th class="bg1">{{ $feriados }}</th>
        <th class="bg1">{{ $faltas }}</th>
        <th class="bg1">{{ $horas_extras }}</th>
        {{-- <th class="bg1">{{ $domingos }}</th> --}}

        <th class="bg2">{{ $horas_noturnas_ma }}</th>
        <th class="bg2">{{ $feriados_ma }}</th>
        <th class="bg2">{{ $faltas_ma }}</th>
        <th class="bg2">{{ $horas_extras_ma }}</th>
        {{-- <th class="bg2">{{ $domingos_ma }}</th> --}}
    </tfoot>
</table>
<div class="row">
    <div class="offset-md-4 col-md-4">
        <button onclick="exporta_loading('Estamos tentando exportar o CSV.', '{{ url('/exporta/totais') }}', 'Totais.csv')" class="btn btn-info btn-sm btn-block" data-toggle="tooltip" title="Clique para exportar os totais em CSV."><i class="fas fa-file-csv"></i> Exportar em CSV</button>
    </div>
</div>
@endsection

@section('scripts')
<script>

    let buttonCommon = {
        exportOptions: {
            columns: ':visible',
        }
    };

    let positions = "<'row mt-2'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6 text-center'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mb-2'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

    let options = {
        "dom": positions,
        "ordering": true,
        "info":     true,
        "paging": false,
        "columnDefs": [
            {"orderable": false, "targets": 'no-sort'},
        ],
        "searching": true,
        "responsive": {
            breakpoints: [
                { name: 'desktop-xl',  width: Infinity },
                { name: 'desktop-lg',  width: 2559 },
                { name: 'desktop',  width: 1439 },
                { name: 'tablet-l', width: 1023 },
                { name: 'tablet-p', width: 767 },
                { name: 'mobile-l', width: 479 },
                { name: 'mobile-p', width: 319 }
            ]
        },
        "order": [],
        "responsive": true,
        "buttons": [
            $.extend(true, {}, buttonCommon, {
                extend: 'print',
                text: '<i class="fas fa-print"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para imprimir toda a tabela.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para copiar toda a tabela.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como CSV.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como XLS.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como PDF.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'colvis',
                text: '<i class="fas fa-eye"></i>',
                titleAttr: 'Clique aqui para alterar a visualização de colunas.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                },
            }),
        ],
        "fixedHeader": {
            "header": true,
            "footer": true,
            headerOffset: $('#navbar_resumo_ponto').height()+15
        },
        "language": {
            "search": "<i class='fas fa-search'></i> Pesquisar:",
            "zeroRecords": "Total não encontrado!",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first":      "<i class=\"fas fa-angle-double-left\"></i>",
                "last":       "<i class=\"fas fa-angle-double-right\"></i>",
                "next":       "<i class=\"fas fa-angle-right\"></i>",
                "previous":   "<i class=\"fas fa-angle-left\"></i>"
            },
            "info": "Mostrando _START_ até _END_ de _TOTAL_ totais.",
            "infoFiltered": " Filtrado de _MAX_ totais.",
            "emptyTable": "Nenhum total na tabela.",
            "zeroRecords": "Nenhum total encontrado.",
            "infoEmpty": "Nenhuma total a ser mostrado.",
            "buttons": {
                copySuccess: {
                    1: "Copiada uma linha para área de transferência.",
                    _: "Copiadas %d linhas para a área de transferência."
                },
                copyTitle: "Copiado para a área de transferência.",

            }
        },
    };

    /**
     * Inicialização da DataTable.
    */

    let table = $('#tabela-totais').DataTable(options);

    function resize(columns){
        let primeiras_colunas = [0, 1];
        let colunas_mes_atual = [2, 3, 4, 5];
        let colunas_mes_anterior = [6, 7, 8, 9];

        let show_colunas_mes_aterior = false;
        colunas_mes_anterior.forEach((el, i, arr) => {
            if (columns[el] === true) show_colunas_mes_aterior = true;
        });
        if(show_colunas_mes_aterior){
            if($('#complex_header3').hasClass('hide')){
                $('#complex_header3').removeClass('hide');
            }
        }else{
            $('#complex_header3').addClass('hide');
        }

        let show_colunas_mes_atual = false;
        colunas_mes_atual.forEach((el, i, arr) => {
            if (columns[el] === true) show_colunas_mes_atual = true;
        });
        if(show_colunas_mes_atual){
            if($('#complex_header2').hasClass('hide')){
                $('#complex_header2').removeClass('hide');
            }
        }else{
            $('#complex_header2').addClass('hide');
        }

        if(columns[0] === false && columns[1] === true){
            if(!$('#complex_header1').hasClass('hide'))
                $('#complex_header1').addClass('hide');
        }else{
            if($('#complex_header1').hasClass('hide'))
                $('#complex_header1').removeClass('hide');
        }

        table.responsive.recalc();
    }

    /**
     * Ajuste de complex_headers para responsividade
    */
    table.on('responsive-resize', function(e, datatable, columns){
        resize(columns);
    });

    let responsive_hidden_columns = table.columns().responsiveHidden();
    resize(responsive_hidden_columns);
</script>
@endsection

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - Indiana</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">

        <div id="loaderx"></div>
        <div class="progress d-none" style="height: 5px;" id="progress_bar">
            <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%; z-index: 1061;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <nav class="navbar navbar-dark bg-dark navbar-expand-lg sticky-top" role="navigation" id="navbar_resumo_ponto">
            {{-- <div class="container-fluid"> --}}
                <a class="navbar-brand" href="{{ URL::to('/') }}">
                    <img src="{{ asset('img/logo.png') }}"  width="180" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    @if (Auth::check())

                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL::to('/') }}">
                                <i class="fas fa-list-ul"></i> Lançamentos
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL::to('/divergencias') }}"><i class="fas fa-exclamation"></i> Painel de NC</a>
                        </li>

                        @cannot('eAdministrador', App\User::class)

                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL::to('/folhaextra') }}"><i class="fas fa-print"></i> Impressão do ponto</a>
                        </li>

                        @endcannot

                        @can('eAdministrador', App\User::class)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle @if(Route::is('usuarios.index')) active @endif" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user-lock"></i> Administrador
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="nav-link-dropdown dropdown-item" href="{{ URL::to('/totais') }}"><i class="fas fa-check"></i> Totais</a>
                                <a class="nav-link-dropdown dropdown-item" href="{{ URL::to('/fechamentos') }}"><i class="fas fa-check-double"></i> Fechamentos</a>
                                <a class="nav-link-dropdown dropdown-item" href="{{ URL::to('/folhaextra') }}"><i class="fas fa-print"></i> Impressão do ponto</a>
                                <div class="dropdown-divider"></div>
                                <a class="nav-link-dropdown dropdown-item @if(Route::is('usuarios.index')) active @endif" href="{{ URL::to('/usuarios') }}">
                                    <i class="fas fa-users"></i> Usuários
                                </a>
                            </div>
                        </li>

                        @endcan
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <strong><i class="fas fa-user"></i> {{ Auth::user()->usuario }}</strong> <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt text-danger"></i> {{ __('Sair') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>

                    @endif

                </div><!-- /.navbar-collapse -->
            {{-- </div><!-- /.container-fluid --> --}}
        </nav>

        <div id="wrapper">
            <div class="container-fluid">

                <!-- Notifications -->
                @include('notifications')
                <!-- ./ notifications -->

                @section('page-header')

                @show

                <!-- Content -->
                @yield('content')
                <!-- ./ content -->

            </div>

        </div> <!-- wrapper -->

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Stacked Scripts -->
    @yield('scripts')

    <!-- Tabela Ponto -->
    @yield('tabela_ponto')
</body>
</html>

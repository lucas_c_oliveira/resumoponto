@extends('layouts.app')

@section('page-header')
    <div class="row">
        <div class="jumbotron w-100 p-0 pt-3 mt-2 mb-1">
            <div class="form-group row mb-0">
                <label for="filial" class="col-lg-4 form-group col-form-label">
                    <h4 class="mb-0"><i class="fas fa-exclamation"></i> Painel de NC</h4>
                </label>
                <div class="offset-lg-4 col-lg-4 form-group text-center">
                    <div class="dropdown btn-block">
                        <button class="btn btn-info btn-sm btn-block dropdown-toggle" type="button" id="periodos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-second="tooltip" title="Escolha o período.">
                            <i class="fas fa-calendar-alt"></i> {{ $periodos['periodo_selecionado']['mes_extenso'] }} de {{ $periodos['periodo_selecionado']['ano'] }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="periodos" id="periodos-label-dropdown-menu">
                            @foreach ($periodos['periodos'] as $periodo)
                            <a class="dropdown-item @if($periodos['periodo_selecionado']['ano_mes'] == $periodo->ano_mes ) active @endif" href="#" onclick="applyFilter('Estamos aplicando o filtro para o período de {{ $periodo->mes_extenso }} de {{ $periodo->ano }}.', '{{URL::to('/filtrar_por_periodo')}}?ano_mes={{ $periodo->ano_mes }}')">
                                <i class="fas fa-calendar-alt"></i> {{ $periodo->mes_extenso }} de {{ $periodo->ano }}
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <table class="table display dt-responsive table-sm" id="table-divergencias" style="width: 100%">
        <thead class="bg-info text-white">
            <th class="tablet-p tablet-l desktop desktop-lg"><i class="fas fa-layer-group"></i> Região</th>
            <th class="all"><i class="far fa-building"></i> Filial</th>
            <th class="tablet-p tablet-l desktop desktop-lg"><i class="fas fa-vector-square"></i> Setor</th>
            <th class="no-sort tablet-l desktop desktop-lg"><i class="fas fa-map-marker-alt"></i> Ponto</th>
            <th class="no-sort tablet-l desktop desktop-lg"><i class="fas fa-exclamation"></i> Pgmt. Errado / Atestado</th>
            <th class="tablet-l desktop desktop-lg"><i class="fas fa-calendar-day"></i> Dt. Entrega</th>
            <th class="tablet-l desktop desktop-lg"><i class="fas fa-calendar-day"></i> Dt. Alteração</th>
            <th class="tablet-l desktop desktop-lg"><i class="fas fa-user"></i> Usuário</th>
            <th class="no-sort tablet-l desktop desktop-lg"><i class="fas fa-arrow-down"></i> Ação</th>
        </thead>
        <tbody>
            @foreach ($divergencias as $divergencia)
                <tr>
                    <td>{{ $divergencia->regiao }}</td>
                    <td>
                        {{ str_pad($divergencia->nr_fil, 3, "0", STR_PAD_LEFT) }} - {{ $divergencia->filial }}
                    </td>
                    <td>{{ $divergencia->setor }}</td>
                    <td><img src="{{ asset('images/'. $divergencia->ponto . '.png') }}"></td>
                    <td>
                        @if($divergencia->holerite == 'P')
                        <img src="{{ asset('images/PW.png') }}">
                        @else
                        <img src="{{ asset('images/'. $divergencia->holerite . '.png') }}">
                        @endif
                    </td>
                    <td>
                        @if ($divergencia->dt_entrega)
                            {{ implode('/', array_reverse(explode('-', $divergencia->dt_entrega))) }}
                        @endif
                    </td>
                    <td>
                        {{ implode('/', array_reverse(explode('-', substr($divergencia->ult_alteracao, 0, 10)))) . ' ' . substr($divergencia->ult_alteracao, 11, 8) }}
                    </td>
                    <td>
                        {{ $divergencia->usuario }}
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" title="Ver." onclick="visualizar({{ $divergencia->id }})"><i class="fas fa-eye"></i></button>
                            @if(\Auth::user()->can('eAdministrador', \App\User::class) && \Session::get('f_ano_mes') != date('Ym'))
                            <button type="button" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Editar." onclick="editar({{ $divergencia->id }})"><i class="fas fa-pencil-alt"></i></button>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="modal fade bd-example-modal-lg" id="visualizacaoModal" tabindex="-1" role="dialog" aria-labelledby="Modal visualização." aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Fechar" data-toggle="tooltip" title="Fechar.">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="visualizacao_body">
                        <div class="col-md-12 text-center">
                            <h3>Carregando...</h3>
                        </div>
                        <div class="col-md-12 mt-2 text-center">
                            <div class="spinner-border text-primary" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Fechar."><i class="fas fa-close"></i> Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="edicaoModal" tabindex="-1" role="dialog" aria-labelledby="Modal edição." aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Fechar" data-toggle="tooltip" title="Fechar.">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <form method="POST" id="editForm" action="{{URL::to('/divergencias')}}/">
                    @method('PUT')
                    @csrf
                    <div class="modal-body">
                        <div class="row" id="edicao_body">
                            <div class="col-md-12 text-center">
                                <h3>Carregando...</h3>
                            </div>
                            <div class="col-md-12 mt-2 text-center">
                                <div class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" data-toggle="tooltip" title="Fechar."><i class="fas fa-close"></i> Fechar</button>
                        <button type="button" onclick="createLoading('editForm', 'Estamos tentando salvar esta Não Conformidade.')" class="btn btn-primary" id="modal-save"><i class="fas fa-save"></i> Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    /**
     * Configurações de incialização da DataTable.
    */

    function initializeOptions(breakpoint){
        if(breakpoint == 'xSmall' || breakpoint == 'small'){
            return "full";
        }else if(breakpoint == 'medium'){
            return "simple_numbers";
        }else if(breakpoint == 'large' || breakpoint == 'xLarge'){
            return "full_numbers";
        }
    }

    let names = [
        'Região',
        'Filial',
        'Setor',
        'Ponto',
        'Pgmt. Errado / Atrasado',
        'Dt. Entrega',
        'Dt. Alteração',
        'Usuário',
        '',
    ];

    let buttonCommon = {
        exportOptions: {
            columns: ':visible',
            format: {
                header: function(data, colindex, trDOM, node){
                    return names[colindex];
                }
            }
        }
    };

    let positions = "<'row mb-2 mt-2'<'col-sm-12 col-md-12 text-center'B>>" +
        "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mb-2'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

    let options = {
        "dom": positions,
        "paging":   true,
        "paginationType": initializeOptions(bsBreakpoints.getCurrentBreakpoint()),
        "ordering": true,
        "info":     true,
        "columnDefs": [
            {"orderable": false, "targets": 'no-sort'},
        ],
        "searching": true,
        "responsive": {
            breakpoints: [
                { name: 'desktop-xl',  width: Infinity },
                { name: 'desktop-lg',  width: 2559 },
                { name: 'desktop',  width: 1439 },
                { name: 'tablet-l', width: 1023 },
                { name: 'tablet-p', width: 767 },
                { name: 'mobile-l', width: 479 },
                { name: 'mobile-p', width: 319 }
            ]
        },
        "order": [],
        "responsive": true,
        "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "Todos"]],
        "buttons": [
            $.extend(true, {}, buttonCommon, {
                extend: 'print',
                text: '<i class="fas fa-print"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para imprimir toda a tabela.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para copiar toda a tabela.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como CSV.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como XLS.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                title: 'Divergências - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como PDF.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                }
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'colvis',
                text: '<i class="fas fa-eye"></i>',
                titleAttr: 'Clique aqui para alterar a visualização de colunas.',
                className: 'text-light btn-info btn-sm',
                attr: {
                    'data-toggle': 'tooltip',
                },
                columnText: function(dt, idx, title){
                    return names[idx];
                }
            }),
        ],
        "fixedHeader": {
            "header": true,
            headerOffset: $('#navbar_resumo_ponto').height()+15
        },
        "language": {
            "search": "<i class='fas fa-search'></i> Pesquisar:",
            "zeroRecords": "Não conformidade não encontrado!",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first":      "<i class=\"fas fa-angle-double-left\"></i>",
                "last":       "<i class=\"fas fa-angle-double-right\"></i>",
                "next":       "<i class=\"fas fa-angle-right\"></i>",
                "previous":   "<i class=\"fas fa-angle-left\"></i>"
            },
            "info": "Mostrando _START_ até _END_ de _TOTAL_ não conformidades.",
            "infoFiltered": " Filtrado de _MAX_ não conformidades.",
            "emptyTable": "Nenhum usuaário na tabela.",
            "zeroRecords": "Nenhuma não conformidades encontrada.",
            "infoEmpty": "Nenhuma não conformidade a ser mostrada.",
            "buttons": {
                copySuccess: {
                    1: "Copiada uma linha para área de transferência.",
                    _: "Copiadas %d linhas para a área de transferência."
                },
                copyTitle: "Copiado para a área de transferência.",

            }
        },
        "drawCallback": function(){
            paginationButtons();
        }
    };

    let positions2 = "<'row mb-2 mt-2'<'col-sm-12 col-md-12 text-center'B>>" +
        "<'row'<'col-sm-12 col-md-12 col-lg-6'l><'col-sm-12 col-md-12 col-lg-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mb-2'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

    let options2 = {
        "dom": positions2,
        "paging":   true,
        "paginationType": initializeOptions(bsBreakpoints.getCurrentBreakpoint()),
        "ordering": true,
        "info":     true,
        "columnDefs": [
            {"orderable": false, "targets": 'no-sort'},
        ],
        "searching": true,
        "responsive": {
            breakpoints: [
                { name: 'desktop-xl',  width: Infinity },
                { name: 'desktop-lg',  width: 2559 },
                { name: 'desktop',  width: 1439 },
                { name: 'tablet-l', width: 1023 },
                { name: 'tablet-p', width: 767 },
                { name: 'mobile-l', width: 479 },
                { name: 'mobile-p', width: 319 }
            ]
        },
        "order": [],
        "responsive": true,
        "lengthMenu": [12],
        "buttons": [
            {
                extend: 'print',
                text: '<i class="fas fa-print"></i>',
                title: 'Painel de NC - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para imprimir toda a tabela.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i>',
                titleAttr: 'Clique aqui para copiar toda a tabela.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i>',
                titleAttr: 'Clique aqui para exportar toda a tabela como CSV.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i>',
                titleAttr: 'Clique aqui para exportar toda a tabela como XLS.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i>',
                title: 'Painel de NC - Resumo de Ponto - Indiana',
                titleAttr: 'Clique aqui para exportar toda a tabela como PDF.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fas fa-eye"></i>',
                titleAttr: 'Clique aqui para alterar a visualização de colunas.',
                className: 'text-light btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                },
                attr: {
                    'data-toggle': 'tooltip',
                }
            },
        ],
        "language": {
            "search": "<i class='fas fa-search'></i> Pesquisar:",
            "zeroRecords": "Não conformidade não encontrado!",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first":      "<i class=\"fas fa-angle-double-left\"></i>",
                "last":       "<i class=\"fas fa-angle-double-right\"></i>",
                "next":       "<i class=\"fas fa-angle-right\"></i>",
                "previous":   "<i class=\"fas fa-angle-left\"></i>"
            },
            "info": "Mostrando _START_ até _END_ de _TOTAL_ não conformidades.",
            "infoFiltered": " Filtrado de _MAX_ não conformidades.",
            "emptyTable": "Nenhum usuaário na tabela.",
            "zeroRecords": "Nenhuma não conformidades encontrada.",
            "infoEmpty": "Nenhuma não conformidade a ser mostrada.",
            "buttons": {
                copySuccess: {
                    1: "Copiada uma linha para área de transferência.",
                    _: "Copiadas %d linhas para a área de transferência."
                },
                copyTitle: "Copiado para a área de transferência.",

            }
        },
        "drawCallback": function(){
            paginationButtons();
        }
    };

    /**
     * Inicialização da DataTable.
    */

    let table = $('#table-divergencias').DataTable(options);

    /**
     * Altração da paginação quando necessário.
    */
    function alteraOptions(breakpoint){
        if(breakpoint == 'xSmall' || breakpoint == 'small'){
            options.paginationType = "full";
            table.destroy();
            table = $('#table-divergencias').DataTable(options);
        }else if(breakpoint == 'medium'){
            options.paginationType = "simple_numbers";
            table.destroy();
            table = $('#table-divergencias').DataTable(options);
        }else if(breakpoint == 'large' || breakpoint == 'xLarge'){
            options.paginationType = "full_numbers";
            table.destroy();
            table = $('#table-divergencias').DataTable(options);
        }
    }
    $(window).on('new.bs.breakpoint', function(e){
        alteraOptions(e.breakpoint);
    });

    function paginationButtons(){
       $('a[data-dt-idx]').addClass('btn btn-sm btn-info border-0 text rounded-0');
       $('a.first').removeClass('rounded-0');
       $('a.last').removeClass('rounded-0');
       $('a.first').addClass('rounded-left');
       $('a.last').addClass('rounded-right');
    }

    $('#table-divergencias tbody').on('mouseover', 'tr', function () {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover',
        });
    });

    function visualizar(id){
        $('#visualizacaoModal').find('.modal-title').text("Vizualizar Não Conformidade");
        $('#visualizacao_body').load('{{ url("divergencias") }}/' + id, function(){
            var table_visualizacao = $('#table-visualizacao').DataTable(options2);
            $('#table-visualizacao_length > label').addClass('text-wrap text-center w-100');
            $('#table-visualizacao_filter > label').addClass('text-wrap text-center w-100');
        });
        $('#visualizacaoModal').modal('toggle');
    }

    $('#visualizacaoModal').on('hidden.bs.modal', function (evt){
        var modal = $(this);
        modal.find('#visualizacao_body').html('<div class="col-md-12 text-center"><h3>Carregando...</h3></div><div class="col-md-12 mt-2 text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
        $('#table-visualizacao').DataTable().clear().destroy();
    });

    function editar(id){
        $('#edicaoModal').find('.modal-title').text("Editar Não Conformidade");
        $('#edicao_body').load('{{ url("divergencias") }}/' + id + '/edit');
        $('#editForm').attr('action', '{{ url("divergencias") }}/' + id);
        $('#edicaoModal').modal('toggle');
    }

    $('#edicaoModal').on('hidden.bs.modal', function (evt){
        var modal = $(this);
        modal.find('#edicao_body').html('<div class="col-md-12 text-center"><h3>Carregando...</h3></div><div class="col-md-12 mt-2 text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div>');
    });
</script>
@endsection

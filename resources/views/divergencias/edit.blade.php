<div class="col-md-6 form-group">
    <label for="nr_fil"><i class="far fa-building"></i> Filial</label>
    <input type="text"
        id="nr_fil"
        class="form-control text-center input-sm"
        value="{{ $divergencia->nr_fil. ' - ' . $divergencia->filial }}"
        readonly>
</div>
<div class="col-md-6 form-group">
    <label for="setor"><i class="fas fa-vector-square"></i> Setor</label>
    <input type="text"
        id="setor"
        class="form-control text-center input-sm"
        value="{{ $divergencia->setor }}"
        readonly>
</div>
@php
    $options = array('P' => 'Pendente', 'R' => 'Recebido', 'C' => 'Conforme')
@endphp
<div class="col-md-6 form-group">
    <label for="ponto"><i class="fas fa-map-marker-alt"></i> Ponto <span class="text-danger">*</span></label>
    <select id="ponto"
        name="ponto"
        class="form-control input-sm"
        value="{{ old('ponto', $divergencia->ponto) }}">
        @foreach ($options as $option => $value)
            <option value="{{$option}}"
                @if ($option == old('ponto', $divergencia->ponto))
                    selected="selected"
                @endif
            >{{$value}}</option>
        @endforeach
    </select>
</div>
<div class="col-md-6 form-group">
    <label for="holerite"><i class="fas fa-exclamation"></i> Pgto Errado / Atestado <span class="text-danger">*</span></label>
    <select id="holerite"
        name="holerite"
        class="form-control input-sm">
        @foreach ($options as $option => $value)
            <option value="{{$option}}"
                @if ($option == old('holerite', $divergencia->holerite))
                    selected="selected"
                @endif
            >{{$value}}</option>
        @endforeach
    </select>
</div>
<div class="col-md-12 form-group">
    <label for="observacao"><i class="fas fa-envelope-open-text"></i> Observação <span class="text-danger">* <sup>(min. 3 caracteres)</sup></span></label>
    <textarea id="observacao" name="observacao" class="form-control">{{ old('observacao', $divergencia->observacao) }}</textarea>
</div>
<div class="col-md-12">
    <p class="mb-0 text-danger">* Campo obrigatório.</p>
</div>

<div class="col-md-12">
    <h5 style="font-size: 1rem !important;">Filial {{ $divergencia->nr_fil }} - {{ $divergencia->filial }} | Setor {{ $divergencia->setor }} </h5>
</div>
<div class="col-md-12">
<table class="table table-striped table-hover table-sm" id='table-visualizacao' width="100%">
    <thead class="bg-info text-white">
        <tr>
            <th class="all"><i class="far fa-calendar"></i> Período</th>
            <th class="no-sort tablet-p tablet-l desktop desktop-lg"><i class="fas fa-map-marker-alt"></i> Ponto</th>
            <th class="no-sort tablet-p tablet-l desktop desktop-lg"><i class="fas fa-exclamation"></i> Pgto. Errado / Atestado</th>
            <th class="no-sort tablet-l desktop desktop-lg"><i class="fas fa-envelope-open-text"></i> Observação</th>
        </tr>
    </thead>
    <tbody>
        @foreach($divergencias_periodo as $periodo)
        <tr>
            <td>
                {{ substr($periodo->ano_mes, 4, 2) }}/{{ substr($periodo->ano_mes, 0, 4) }}
            </td>
            <td>
                <img src="{{ asset('images/'. $periodo->ponto . '.png') }}">
            </td>
            <td>
                <img src="{{ asset('images/'. $periodo->holerite . '.png') }}">
            </td>
            <td style="font-size:12px; text-align:left">{{ $periodo->observacao }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>

require('./bootstrap');

/*************************************/
/** DEFININDO IMPORTAÇÕES DO jQuery **/
/*************************************/

import $ from 'jquery';
window.$ = window.jQuery = $;

/**************************************/
/** CARREGANDO INFORMAÇÕES DO LOADER **/
/**************************************/

$(window).on('load',function() {
    $("#loaderx").fadeOut("slow");
});

$('#send_login_button').on('click', function(){
    $('#loaderx').fadeIn("slow");
});

$('.nav-link:not(.dropdown-toggle)').on('click', function(){
    $('#loaderx').fadeIn("slow");
});

$('.nav-link-dropdown').on('click', function(){
    $('#loaderx').fadeIn("slow");
});

/*******************************************/
/** IMPORTAÇÃO DE UTILIZAÇÃO DAS TOOLTIPS **/
/*******************************************/

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle-second="tooltip"]').tooltip();
});

/*******************************************************/
/** IMPORTAÇÃO DE JSZip PARA UTILIZAÇÃO DO DATATABLES **/
/*******************************************************/

window.JSZip = require('jszip');

/*********************************************************/
/** IMPORTAÇÃO DE PDFMake PARA UTILIZAÇÃO DO DATATABLES **/
/*********************************************************/

let pdfMake = require('pdfmake/build/pdfmake');
let pdfFonts = require('pdfmake/build/vfs_fonts');
pdfMake.vfs = pdfFonts.pdfMake.vfs;

/*******************************************************/
/** IMPORTAÇÃO DE UTILIZAÇÃO DOS DATATABLES & PLUGINS **/
/*******************************************************/

import 'datatables.net-bs4';
import 'datatables.net-buttons-bs4';
import 'datatables.net-fixedheader-bs4';
import 'datatables.net-responsive-bs4';
import 'datatables.net-buttons/js/buttons.print';
import 'datatables.net-buttons/js/buttons.colVis';
import 'datatables.net-buttons/js/buttons.html5';

/************************************************************/
/** IMPORTAÇÃO DE PACOTE DETECTOR DE BREAKPOINTS BOOTSTRAP **/
/************************************************************/

window.bsBreakpoints = require('bs-breakpoints');

// Inicialização do pacote
bsBreakpoints.init();

/*******************************/
/** IMPORTAÇÃO DO SWEET ALERT **/
/*******************************/

import Swal from 'sweetalert2';

window.Swal = Swal;

window.deleteConfirm = function(text, formId, textInfo, deleteIcon){
    if(deleteIcon == 'user-times'){
        deleteIcon = '<i class="fas fa-user-times"></i>';
    }else if(deleteIcon == 'trashcan'){
        deleteIcon = '<i class="far fa-trash-alt"></i>';
    }
    Swal.fire({
        icon: 'warning',
        title: 'Tem certeza?',
        text: text,
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonText: deleteIcon + ' Excluir',
        cancelButtonText: '<i class="fas fa-times"></i> Cancelar',
        buttonsStyling: false,
        customClass: {
            confirmButton: "btn btn-danger mr-2",
            cancelButton: "btn btn-info"
        }
    })
    .then((result) => {
        $('#progress_bar').removeClass('d-none');
        if(result.isConfirmed){
            Swal.fire({
                icon: 'info',
                title: 'Aguarde...',
                text: textInfo,
                showConfirmButton: false,
                showCancelButton: false,
                showCloseButton: false,
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading();
                }
            });
            document.getElementById(formId).submit();
        }
    });
}

window.createLoading = function(formId, textInfo){
    $('#progress_bar').removeClass('d-none');
    Swal.fire({
        icon: 'info',
        title: 'Aguarde...',
        text: textInfo,
        showConfirmButton: false,
        showCancelButton: false,
        showCloseButton: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: () => {
            Swal.showLoading();
        }
    });
    document.getElementById(formId).submit();
}

window.applyFilter = function(textInfo, link){
    $('#progress_bar').removeClass('d-none');
    Swal.fire({
        icon: 'info',
        title: 'Aguarde...',
        text: textInfo,
        showConfirmButton: false,
        showCancelButton: false,
        showCloseButton: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: () => {
            Swal.showLoading();
        }
    });
    window.location.assign(link);
}

window.exporta_loading = function(textInfo, link, nome_arquivo){
    $('#progress_bar').removeClass('d-none');
    Swal.fire({
        icon: 'info',
        title: 'Aguarde...',
        text: textInfo,
        showConfirmButton: false,
        showCancelButton: false,
        showCloseButton: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: () => {
            Swal.showLoading();
        }
    });
    axios.get(link, {
        responseType: 'blob',
    })
    .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', nome_arquivo);
        document.body.appendChild(link);
        link.click();
        Swal.fire({
            icon: 'success',
            title: 'Pronto!',
            text: 'Exportação realizada com sucesso.',
            showConfirmButton: true,
            showCancelButton: false,
            showCloseButton: true,
            confirmButtonText: '<i class="fas fa-check-double"></i> Show!',
            buttonsStyling: false,
            customClass: {
                confirmButton: "btn btn-success btn-sm",
            },
            didOpen: () => {
                Swal.hideLoading();
            }
        });
    })
    .catch((error) => {
        Swal.fire({
            icon: 'error',
            title: 'Eita...',
            text: 'Exportação não realizada... ',
            showConfirmButton: true,
            showCancelButton: false,
            showCloseButton: true,
            confirmButtonText: '<i class="fas fa-exclamation-circle"></i> Entendido...',
            buttonsStyling: false,
            customClass: {
                confirmButton: "btn btn-info btn-sm",
            },
            didOpen: () => {
                Swal.hideLoading();
            }
        });
    });
}

/**************************************/
/** IMPORTAÇÃO DO jQuery Mask Plugin **/
/**************************************/
import 'jquery-mask-plugin';

/***********************************************/
/** IMPORTAÇÃO DO jsPDF e do plugin AutoTable **/
/***********************************************/
import { jsPDF } from 'jspdf';
import 'jspdf-autotable';
import axios from 'axios';
window.jsPDF = jsPDF;

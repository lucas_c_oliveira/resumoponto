<p align="center"><a href="https://bitbucket.org/lucas_c_oliveira/resumoponto/src/master/" target="_blank"><img src="https://bitbucket.org/lucas_c_oliveira/resumoponto/raw/11d375ba2f2ca27598afe6f123c44f816468d629/public/img/logo.png" width="400"></a></p>

## Sobre o Resumo de Ponto

Resumo de Ponto é um sistema legado, migrado e reconstruido para Laravel 6.0 a partir de um código legado Laravel 4.0 construído em monobloco por Leonardo Caitano. Esse sistema de ponto é responsável por receber lançamentos de Horas Extras, Horas Noturnas, Feriados e Faltas dos funcionários e gerar Folhas de Ponto manuais para serem preenchidas e entregues.

No quesito de responsividade o sistema se encontra totalmente responsivo. Contudo, este sistema é melhor visualizado em telas com largura a partir de 1440px.

O sistema conta com 6 telas principais e não utiliza framework reativo Front-End. Contudo, conta com Bootstrap e outros pacotes NPM Javascript que compõe todo o código. As telas são:

- Lançamentos;
- Painel de NC (Não Conformidades);
- Totais;
- Fechamentos;
- Impressão de Ponto;
- Usuários.

Dentre essas telas, as de Totais, Fechamentos e Usuários estão disponíveis somente para Administradores.

Há ainda algumas procedures Executando dentro do código - Herança de Leonardo Caitano (qualquer problema com as procedures favor consultar o mesmo rsrs ou ainda o Gustavo Postigo).

Há dentro do projeto migração para a criação das tabelas de:

 - Usuários;
 - Password Reset (Não utilizado / Regra de Negócio (RN) do código original);
 - Failed Jobs (Não utilizado / RN do código original);
 - Ponto;
 - Status Lanc;
 - Fechamentos;

Nem todas as tabelas estão contempladas com migrations. Algumas tabelas são temporárias, criadas a partir das procedures e outras ainda são Exibições (Views) que não podem ser alteradas.

Dentre os Models criados estão:

 - Divergencias;
 - Fechamentos;
 - Filial;
 - Funcionarios;
 - Ponto;
 - Status;
 - User;
 - VDivergencias;
 - VPonto.

Todos esses Models são entidades necessárias no código. Contudo, nem todas as entidades utilizadas pelos controladores tem Models definidos, como, por exemplo, as tabelas SRA010 que aparecem em joins em algumas partes do código e não tiveram models criados para evitar a fadiga e encurtar tempo de produção da migração da aplicação.

Os Controladores da Aplicação, além dos autenticação, são:

 - DivergenciaController.php;
 - FechamentosController.php;
 - FilialController.php;
 - HomeController.php;
 - PontoController.php;
 - TotaisController.php;
 - UserController.php;

Por falar em Autenticação, esse projeto conta con Autenticação AD liberada através do registro do usuário e sua respectiva matrícula através da tela de Usuários. Somente Usuários registrados tem acesso a este sistema. O registro do usuário pode acontecer a qualquer momento após a sincronização deste sistema com o Protheus. Contudo o processo de sincronização pode chegar a durar até um mês, quando o usuário pode começar a ter acesso ao sistema.

Para ajustar a tabela de usuários no banco de dados de acordo com as novas necessidades do ADLDAP foi necessária criar uma Command que deva ser rodada, antes de colocar este serviço em produção. No momento este sistema encontra-se somente em homologação para testes pelo Leonardo Caitano, uma vez que a Suelem se encontra de férias. Informações adicionais de como rodar essa Command se encontrará na área de Instalação do serviço para alteração Local.

As rotas deste sistema são:

 - /;
 - /login (POST);
 - /logout (POST);
 - /exporta/lancamentos/{formato} (GET);
 - /usuarios (Resource);
 - /filtrar_por_filial (GET).
 - /filtrar_por_periodo (GET).
 - /filtrar_por_setor (GET).
 - /divergencias (Resource);
 - /totais (GET);
 - /exporta/totais (GET);
 - /fechamentos (GET);
 - /exporta/fechamentos/{formato} (GET);
 - /exporta/transferidos/{formato} (GET);
 - /folhaextra (GET).

Nenhuam das rotas é API.

## Instalação da Aplicação para alteração em ambiente Local

Todos os requisitos para operar com uma aplicação laravel devem ser respeitados. Isso inclui Composer, Node.js e, se desejar, um gerenciador de Apache. Não se esqueça de instalar o SQLServer no seu gerenciados apache e PHP.
O PHP utilizado para desenvolviemtno foi o 7.3.19.
Rode:

 - composer install;
 - npm install;
 - cp .env.example .env;
 - npm run watch;
 - cp extras/AuthenticatesUsers.php vendor/laravel/framework/src/Illuminate/Foundation/Auth/AuthenticatesUsers.php;

Pronto. Você está pronto para começar.

## License

Codigo proprietário da Farmácia Indiana. Propriedade Intelectual de Leonardo Caitano e Lucas Carvalho.

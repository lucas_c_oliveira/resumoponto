<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;

trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        // Checa se o usuário está registrado na tabela de usuários;
        $user = User::where('usuario', $request->input('usuario'))->first();
        if (!$user){
            // Se não o estiver retorne um erro.
            Log::info('Usuário não cadatrado tentou acesso: ' . $request->input('usuario') . ".");
            throw ValidationException::withMessages([
                $this->username() => ['Usuário não cadastrado no sistema. Entre em contato com o DP e solicite seu cadastro.'],
            ]);
        }
        // Checa se o usuário tem funcionário registrado na Folha Gerencial com base em sua matrícula;
        $funcionario = DB::table('fp.dbo.v_funcionarios_unicos')->where('matricula', '=', $user->matricula)->first();
        if(!$funcionario){
            // Se não tiver retorne um erro;
            Log::info('Usuário com matricula inválida tentou acesso: ' . $request->input('usuario') . ".");
            throw ValidationException::withMessages([
                $this->username() => ['Matrícula do usuário inválida. Entre em contato com o DP e solicite alteração.'],
            ]);
        }

        // Tenta fazer Login;
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );

    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
        // Checa se o usuário tem funcionário registrado na Folha Gerencial com base em sua matrícula;
        $funcionario = DB::table('fp.dbo.v_funcionarios_unicos')->where('matricula', '=', $user->matricula)->first();

        // Se conseguir atualiza o usuário;
        $user->update([
            'nome' => $funcionario->nome,
            'cargo' => $funcionario->cargo,
            'nr_fil' => $funcionario->nr_fil,
            'setor' => $funcionario->setor
        ]);

        // Registra filtros de sessão;
        Session::put('f_nr_fil', Auth::user()->nr_fil);
        Session::put('f_ano_mes', date('Ym'));

        // Registra Log de login;
        Log::info(Auth::user()->nome . " fez Login .");
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'usuario';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return $this->loggedOut($request) ?: redirect('/');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        Session::forget('f_ano_mes');
        Session::forget('f_nr_fil');
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}

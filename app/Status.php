<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //

    /**
     * Quando a chave primária for composta,
     * deve-se desativar a incrementação.
     */
    public $incrementing = false;

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $table = 'status_lanc';

    protected $primaryKey = array('ano_mes', 'nr_fil');

    protected $fillable = [
        'ano_mes',
        'nr_fil',
        'status'
    ];
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;
use Illuminate\Support\Facades\Log;

class DatabaseAdjustment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:adjustment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajusta o banco de dados para que esta de acordo com as novas identidades do ADLDAP.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        Log::info('Rodando o database adjustment.');
        /* Schema::table('usuarios', function(Blueprint $table){
            $table->string('objectguid')->nullable();
            $table->string('password')->nullable();
            $table->timestamps();
            $table->rememberToken();
        }); */
        User::where('usuario', 'lucas.oliveira')->first()->update([
            'setor' => 'DEPARTAM PESSOAL',
            'cargo' => 'GERENTE DPTO PESSOAL',
            'nr_fil' => '999',
            'matricula' => '203621',
        ]);
    }
}

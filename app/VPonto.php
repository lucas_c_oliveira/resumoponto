<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VPonto extends Model
{
    //
    protected $table = 'v_ponto_imp_protheus';

    protected $primaryKey = 'id';

    /**
     * Quando a tabela for uma view,
     * deve-se desativar a incrementação.
     */
    public $incrementing = false;

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $fillable = [
        'id',
        'matricula',
        'nome',
        'nr_fil',
        'catfunc',
        'sindica',
        'situacao',
        'ano_mes',
        'dias_ferias',
        'dias_atestado',
        'faltas_ma',
        'horas_extras_ma',
        'horas_noturnas',
        'v_horas_noturnas',
        'feriados',
        'v_feriados',
        'faltas',
        'v_faltas',
        'horas_extras',
        'v_horas_extras',
        'horas_noturnas_ma',
        'v_horas_noturnas_ma',
        'feriados_ma',
        'v_feriados_ma',
        'domingos',
        'v_domingos',
        'domingos_ma',
        'v_domingos_ma',
        'obs',
    ];
}

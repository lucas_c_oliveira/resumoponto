<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divergencias extends Model
{
    //
    protected $table = 'divergencias';

    protected $primaryKey = 'id';

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $fillable = [
        'id',
        'setfil_id',
        'ponto',
        'holerite',
        'observacao',
        'dt_entrega',
        'ult_alteracao',
        'usuario',
    ];
}

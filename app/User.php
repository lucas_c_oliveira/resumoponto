<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Table name
    */
    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome', 'usuario', 'objectguid', 'password', 'matricula', 'cargo', 'nr_fil', 'setor', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * Remember Token removido pois não há na tabela.
     * @var array
     */
    protected $hidden = [
        'password', /* 'remember_token', */
    ];

    /**
     * The attributes that should be cast to native types.
     * Função comentada pois ná há campo na tabela.
     * @var array
     */
    /* protected $casts = [
        'email_verified_at' => 'datetime',
    ]; */


}

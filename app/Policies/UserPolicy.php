<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any Users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if (!is_null($user)) {
            if (($user->setor == 'GESTÃO DE PESSOAS')
                or ($user->setor == 'DEPARTAM PESSOAL')
                or ($user->setor == 'ASSIST RH')
                or ($user->setor == 'ASSIST. DEPARTAMENTO PESSOAL')
                or ($user->setor == 'ESCRITORIO DA QUALIDADE')
            ) {
                return Response::allow();
            }
        }
        return Response::deny();
    }

    /**
     * Determine whether the user can view the User.
     *
     * @param  \App\User  $user
     * @param  \App\User  $User
     * @return mixed
     */
    public function view(User $user, User $User)
    {
        //
        return Response::deny();
    }

    /**
     * Determine whether the user can store Users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function store(User $user)
    {
        //
        if (!is_null($user)) {
            if (($user->setor == 'GESTÃO DE PESSOAS')
                or ($user->setor == 'DEPARTAM PESSOAL')
                or ($user->setor == 'ASSIST RH')
                or ($user->setor == 'ASSIST. DEPARTAMENTO PESSOAL')
                or ($user->setor == 'ESCRITORIO DA QUALIDADE')
            ) {
                return Response::allow();
            }
        }
        return Response::deny();
    }

    /**
     * Determine whether the user can update the User.
     *
     * @param  \App\User  $user
     * @param  \App\User  $User
     * @return mixed
     */
    public function update(User $user)
    {
        //
        return Response::allow();
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $User
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if (!is_null($user)) {
            if (($user->setor == 'GESTÃO DE PESSOAS')
                or ($user->setor == 'DEPARTAM PESSOAL')
                or ($user->setor == 'ASSIST RH')
                or ($user->setor == 'ASSIST. DEPARTAMENTO PESSOAL')
                or ($user->setor == 'ESCRITORIO DA QUALIDADE')
            ) {
                return Response::allow();
            }
        }
        return Response::deny();
    }

    /**
     * Determine whether the user can restore the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $User
     * @return mixed
     */
    public function restore(User $user, User $User)
    {
        //
        return Response::deny();
    }

    /**
     * Determine whether the user can permanently delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $User
     * @return mixed
     */
    public function forceDelete(User $user, User $User)
    {
        //
        return Response::deny();
    }

    /**
     * Determina se o usuário tem privilégios de Administrador.
     *
     * @param  \App\User  $user
     * @return Boolean
     */
    public function eAdministrador(User $user){
        if (!is_null($user)) {
            if (($user->setor == 'GESTÃO DE PESSOAS')
                or ($user->setor == 'DEPARTAM PESSOAL')
                or ($user->setor == 'ASSIST RH')
                or ($user->setor == 'ASSIST. DEPARTAMENTO PESSOAL')
                or ($user->setor == 'ESCRITORIO DA QUALIDADE')
            ) {
                return Response::allow();
            }
        }
        return Response::deny();
    }

    /**
     * Determina se o usuário tem privilégios de Supervisor.
     *
     * @param  \App\User  $user
     * @return Boolean
     */
    public function eSupervisor(User $user){
        if (!is_null($user)) {
            if ($user->cargo == 'SUPERVISOR') {
                return Response::allow();
            }
        }
        return Response::deny();
    }

    /**
     * Determina se o usuário tem privilégios de Gerente do CDI.
     *
     * @param  \App\User  $user
     * @return Boolean
     */
    public function eGerenteCDI(User $user){
        if (!is_null($user)) {
            if ($user->cargo == 'GERENTE CDI') {
                return Response::allow();
            }
        }
        return Response::deny();
    }
}

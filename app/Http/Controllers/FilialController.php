<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filial;
use App\User;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;

class FilialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Illuminate\Support\Collection
     */
    public function index()
    {
        //
        if (Auth::user()->can('eAdministrador', User::class) ||
            Auth::user()->can('eSupervisor', User::class) ||
            Auth::user()->can('eGerenteCDI', User::class)){
            $filiais = Filial::select(['nr_fil', 'filial', 'regiao_id'])
                ->orderBy('nr_fil', 'asc')
                ->get();
            $filial_escolhida = Filial::where('nr_fil', '=', Session::get('f_nr_fil'))
                ->select(['nr_fil', 'filial', 'regiao_id'])
                ->get();
            Log::info(Auth::user()->nome . ' recuperou as filiais com perfil de administrador.');
            return Collection::make(["filiais" => $filiais, "filial_escolhida" => $filial_escolhida[0]]);
        }else{
            $filiais = Filial::select(['nr_fil', 'filial', 'regiao_id'])
                ->where('nr_fil', '=', Session::get('f_nr_fil'))
                ->get();
            $filial_escolhida = Filial::where('nr_fil', '=', Session::get('f_nr_fil'))
                ->select(['nr_fil', 'filial', 'regiao_id'])
                ->get();
            Log::info(Auth::user()->nome . ' recuperou a filial ' . Session::get('f_nr_fil') . ' sem perfil de administrador.');
            return Collection::make(["filiais" => $filiais, "filial_escolhida" => $filial_escolhida[0]]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return abort(404);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Validation\ValidationException;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Autoriza o acesso por parte do usuário;
        $this->authorize('viewAny', User::class);
        // Recupera os usuários;
        $usuarios = User::orderBy('nome', 'asc')->get();
        // Registra Log de index;
        Log::info(Auth::user()->nome . " acessou o index dos usuáros.");
        // Envia os usuários para a View;
        return view('/usuarios', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**
         * Retorna página não existente.
         */
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Autoriza a inserção por parte do usuário.
        $this->authorize('store', User::class);

        // Valida a entrada de registro de usuário.
        $this->validate($request, [
            'matricula' => 'required|numeric|unique:usuarios,matricula',
            'usuario' => 'required|string|max:100|unique:usuarios,usuario',
        ], [
            'matricula.required' => 'A matrícula é obrigatória.',
            'matricula.numeric' => 'A matrícula deve ser um número.',
            'matricula.unique' => 'A matrícula deve ser única na tabela de usuários.',
            'usuario.required' => 'O Usuário AD é obrigatório',
            'usuario.unique' => 'O Usuário deve ser único na tabela de usuários.',
            'usuario.string' => 'O Usuário deve ser um conjunto de caracters.',
            'usuario.max' => 'O Usuário deve possuir no máximo 100 caracteres.',
        ]);

        // Procura na folha de pagametnos pela matrícula da Request.
        $funcionario = DB::table('fp.dbo.v_funcionarios_unicos')
            ->where("matricula", "=", $request->input('matricula'))
            ->get();

        // Verifica se algum usuário foi recuparado com aquela matrícula.
        if (!isset($funcionario[0])){
            // Se não houver nenhum, retorna um erro.
            throw ValidationException::withMessages([
                'matricula' => ['Matrícula do usuário inválida.'],
            ]);
        }

        $funcionario = $funcionario[0];

        // Criação do usuário
        $user = new User;
        $user->create([
            'matricula' => $request->input('matricula'),
            'usuario' => $request->input('usuario'),
            'nome' => $funcionario->nome,
            'cargo' => $funcionario->cargo,
            'nr_fil' => $funcionario->nr_fil,
            'setor' => $funcionario->setor,
        ]);

        // Registra Log de store;
        Log::info(Auth::user()->nome . " registrou o usuário " . $request->input('usuario') . ".");

        // Retorna a view de index novamente;
        return redirect('usuarios')->with(['success' => '<i class="fas fa-user-plus"></i> Usuário registrado com sucesso.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        /**
         * Retorna página não existente.
         */
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        /**
         * Retorna página não existente.
         */
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /**
         * Retorna página não existente.
         */
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Authoriza o usuário autenticado a deletar;
        $this->authorize('delete', User::class);

        // Encontra o usuário a ser deletado;
        $user = User::findOrFail($id);

        // Exclui o usuário
        $user->delete();

        // Registra Log de delete;
        Log::info(Auth::user()->nome . " excluiu o usuário " . $user->usuario . ".");

        // Redireciona para a página de usuários.
        return redirect('usuarios')->with(['success' => '<i class="fas fa-user-times"></i> Usuário excluido com sucesso.']);
    }
}

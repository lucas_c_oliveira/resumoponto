<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ponto;
use App\Status;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Exports\VPontoExport;

use App\Http\Controllers\FilialController;
use App\Http\Controllers\FiltersController;

use Carbon\Carbon;

class PontoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // Recupera as informações de session.
        $anomes = Session::get('f_ano_mes');
        $filial = Session::get('f_nr_fil');

        /**
         * Gera o resumo de ponto:
         *
         * Garante que novos funcionários sejam incluídos na folha
         * para isso essa procedure deve sempre ser executada pela aplicação
         * antes de mostrar a lista de funcionários e habilitar sua edição.
         */

        //if(!$this->bloqueado()){
            DB::statement("exec p_gera_ponto @filial = '" . $filial . "', @anomes = '" . $anomes . "'");
        //}

        /**
         * Lista todos os funcionários da folha de ponto
         * e seus respectivos lançamentos.
         */

        $funcionarios = Ponto::select('ponto.*', 'SRA010_imp_ponto.nome', 'SRA010_imp_ponto.catfunc', 'SRA010_imp_ponto.situacao', 'SRA010_imp_ponto.cod_cbo')
            ->join('SRA010_imp_ponto', function($join) {
                $join->on('SRA010_imp_ponto.matricula', '=', 'ponto.matricula');
                $join->on('SRA010_imp_ponto.nr_fil', '=', 'ponto.nr_fil');
            })
            ->where('ponto.nr_fil', '=', $filial)
            ->where('ponto.ano_mes', '=', $anomes)
            ->orderBy('SRA010_imp_ponto.nome', 'asc')
            ->get();

        /**
         * Recupera status de lançamento;
         */
        $status = Status::where('ano_mes', $anomes)
            ->where('nr_fil', $filial)
            ->first();
        $status_lanc = ($status) ? $status->status : 0;

        /**
         * Recupera a(s) filial(ais) do usuário;
         */
        $filiais = (new FilialController)->index();

        /**
         * Recupera o(s) período(s) a serem analisados;
         */
        $periodos = (new FiltersController)->periodos();

        /**
         * Recupera se a inserção está bloqueada ou não;
         */
        $bloqueado = false;

        /**
         * Recupera se a inserção está bloqueada para funcionario cbo;
         *
         * Atividade não funcionando no código Original.
         */
        foreach($funcionarios as $f){
            $f->bloqueio_cbo = $this->cboBloqueado($f->cod_cbo);
        }

        // Retorna a view
        Log::info(Auth::user() . ' acessou a rota de lançamentos (Home) com filial: ' . Session::get('f_nr_fil') . ' e com ano_mes ' . Session::get('f_ano_mes') . '.');

        // Retorna a view.
        return view('home', compact('funcionarios', 'status_lanc', 'filiais', 'periodos', 'bloqueado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Valida as requests recebidas
        $this->validate($request, [
            'status_lanc' => 'required|integer|min:0|max:2',
            'funcionario' => 'present',
            'funcionario.*.horas_noturnas' => 'present|sometimes|nullable|regex:/[0-9][0-9]:[0-5][0-9]/',
            'funcionario.*.feriados' => 'present|sometimes|nullable|integer|min:1|max:31',
            'funcionario.*.faltas' => 'present|sometimes|nullable|integer|min:1|max:31',
            'funcionario.*.horas_extras' => 'present|sometimes|nullable|regex:/[0-9][0-9]:[0-5][0-9]/',
            'funcionario.*.horas_noturnas_ma' => 'present|sometimes|nullable|regex:/[0-9][0-9]:[0-5][0-9]/',
            'funcionario.*.feriados_ma' => 'present|sometimes|nullable|integer|min:1|max:31',
            'funcionario.*.faltas_ma' => 'present|sometimes|nullable|integer|min:1|max:31',
            'funcionario.*.horas_extras_ma' => 'present|sometimes|nullable|regex:/[0-9][0-9]:[0-5][0-9]/'
        ], [
            'status_lanc.required' => 'O Status de Lançamento é obrigatório.',
            'status_lanc.integer' => 'O Status de Lançamento deve ser um número inteiro.',
            'status_lanc.min' => 'O Status de Lançamento deve ser no mínimo 0.',
            'status_lanc.max' => 'O Status de Lançamento deve ser no máximo 2.',
            'funcionario.present' => 'Os Funcionários devem estar presentes.',
            'funcionario.*.horas_noturnas.present' => 'As horas noturnas do Funcionário deve estar presente.',
            'funcionario.*.horas_noturnas.regex' => 'As horas noturnas do Funcionário deve estar no formato HH:mm.',
            'funcionario.*.feriados.feriados' => 'Os feriados do Funcionário deve estar presente.',
            'funcionario.*.feriados.integer' => 'Os feriados do Funcionário deve ser um número inteiro.',
            'funcionario.*.feriados.min' => 'Os feriados do Funcionário deve ser no mínimo 1.',
            'funcionario.*.feriados.max' => 'Os feriados do Funcionário deve ser no máximo 31.',
            'funcionario.*.faltas.feriados' => 'As faltas do Funcionário deve estar presente.',
            'funcionario.*.faltas.integer' => 'As faltas do Funcionário deve ser um número inteiro.',
            'funcionario.*.faltas.min' => 'As faltas do Funcionário deve ser no mínimo 1.',
            'funcionario.*.faltas.max' => 'As faltas do Funcionário deve ser no máximo 31.',
            'funcionario.*.horas_extras.present' => 'As horas extras do Funcionário deve estar presente.',
            'funcionario.*.horas_extras.regex' => 'As horas extras do Funcionario deve estar no formato HH:mm.',
            'funcionario.*.horas_noturnas_ma.present' => 'As horas noturnas do mês passado do Funcionário deve estar presente.',
            'funcionario.*.horas_noturnas_ma.regex' => 'As horas noturnas do mês passado deve estar no formato HH:mm.',
            'funcionario.*.feriados_ma.feriados' => 'Os feriados do mês passado do Funcionário deve estar presente.',
            'funcionario.*.feriados_ma.integer' => 'Os feriados do mês passado do Funcionário deve ser um número inteiro.',
            'funcionario.*.feriados_ma.min' => 'Os feriados do mês passado do Funcionário deve ser no mínimo 1.',
            'funcionario.*.feriados_ma.max' => 'Os feriados do mês passado do Funcionário deve ser no máximo 31.',
            'funcionario.*.faltas_ma.feriados' => 'As faltas do mês passado do Funcionário deve estar presente.',
            'funcionario.*.faltas_ma.integer' => 'As faltas do mês passado do Funcionário deve ser um número inteiro.',
            'funcionario.*.faltas_ma.min' => 'As faltas do mês passado do Funcionário deve ser no mínimo 1.',
            'funcionario.*.faltas_ma.max' => 'As faltas do mês passado do Funcionário deve ser no máximo 31.',
            'funcionario.*.horas_extras_ma.present' => 'As horas extras do mês passado do Funcionário deve estar presente.',
            'funcionario.*.horas_extras_ma.regex' => 'As horas extras do mês passado do Funcionário deve estar no formato HH:mm.',
        ]);

        // Recupera os filtros de sessão.
        $anomes = Session::get('f_ano_mes');
        $filial = Session::get('f_nr_fil');

        // Para cada funcionário faça:
        foreach ($request->funcionario as $matricula => $ponto){

            // Transforme o ponto em object.
            $ponto = (object) $ponto;

            // Recupera o ponto.
            $pontoObj = Ponto::where('matricula', '=', $matricula)
                ->where('nr_fil', '=', $filial)
                ->where('ano_mes', '=', $anomes)
                ->first();

            // Se o ponto existir atualize-o.
            if($pontoObj){
                $pontoObj->update([
                    'horas_noturnas' => $ponto->horas_noturnas,
                    'faltas' => $ponto->faltas,
                    'feriados' => $ponto->feriados,
                    'horas_extras' => $ponto->horas_extras,
                    'horas_noturnas_ma' => $ponto->horas_noturnas_ma,
                    'faltas_ma' => $ponto->faltas_ma,
                    'feriados_ma' => $ponto->feriados_ma,
                    'horas_extras_ma' => $ponto->horas_extras_ma,
                ]);
            // Se o ponto não existir crie-o.
            }else{
                Ponto::create([
                    'matricula' => $matricula,
                    'ano_mes' => $anomes,
                    'nr_fil' => $filial,
                    'horas_noturnas' => $ponto->horas_noturnas,
                    'faltas' => $ponto->faltas,
                    'feriados' => $ponto->feriados,
                    'horas_extras' => $ponto->horas_extras,
                    'horas_noturnas_ma' => $ponto->horas_noturnas_ma,
                    'faltas_ma' => $ponto->faltas_ma,
                    'feriados_ma' => $ponto->feriados_ma,
                    'horas_extras_ma' => $ponto->horas_extras_ma,
                ]);
            }
        }

        // Verifica se há um status ativo.
        $status = Status::where('ano_mes', '=', $anomes)
            ->where('nr_fil', '=', $filial)
            ->first();

        // Se houver atualize o status ativo.
        if($status){
            Status::where('ano_mes', '=', $anomes)
                ->where('nr_fil', '=', $filial)
                ->update([
                    'status' => $request->status_lanc
                ]);
        // Se não ouver crie o status ativo.
        }else{
            Status::create([
                'ano_mes' => $anomes,
                'nr_fil' => $filial,
                'status' => $request->status_lanc
            ]);
        }

        // Registra o Log.
        Log::info(Auth::user() . ' registrou lançamentos a partir da filial ' . Session::get('f_nr_filial') . ' e do ano_mês' . Session::get('f_ano_mes') . '.');

        // Retorna a View.
        return back()->with('success', '<i class="fas fa-list-ul"></i> Lançamentos atualizados com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return abort(404);
    }

    /**
     * Verifica se a edição dos valores dos Lançamentos se encontram bloqueadas.
     *
     * @return Boolean
     */
    public function bloqueado(){

        // Meses anteriores serão bloqueados para edição
        if(Session::get('f_ano_mes') < date('Ym')){
            return true;
        }

        // Fechamento dia 27 de cada mês
        if(date('d') >= 27 || (date('m') == 2 && date('d') >= 25)){
            return true;
        }

        Log::info(Auth::user() . ' verificou a função bloqueado.');

        return false;

    }

    /**
     * Verifica se a edição está bloqueada ou liberada para algum funcionario.
     * No código original esta função não recebe nenhum parâmetro e, portanto retorna sempre verdadeiro...
     *
     * @param int $cod_cbo
     */
    public function cboBloqueado($cod_cbo){

        // --------------------------------------------
        // Funcionarios bloqueados para lançamento
        // -------------------------------------------
        // CHAMADO 86125

        // CBO SUBGERENTE 142105
        // CBO GERENTE 141415

        $cod_cbo_bloqueados = array('142105', '141415');

        if(in_array($cod_cbo, $cod_cbo_bloqueados)){
            return true;
        }

        Log::info(Auth::user() . ' acessou a função cboBloqueado.');

        return false;
    }

    /**
     * Exporta lancamentos.
     *
     * @param String $formato
     */
    public function exportaLancamentos($formato){
        // Autoria o usuário.
        $this->authorize('eAdministrador', User::class);

        // Verifica o formato
        if(($formato != 'csv') && ($formato != 'xls')){
            return response(json_encode(['message' => 'Formato Inválido']), 403);
        }

        // Se o formato for o CSV.
        if($formato == 'csv'){
            Log::info(Auth::user() . ' exportou a tabela de Lançamentos, através do botão de Exportação ao Fim da Tabela no formato CSV. Filial: ' . Session::get('f_nr_fil') . ' e o ano_mes ' . Session::get('f_ano_mes'));
            return (new VPontoExport)->download('Resumo de Ponto ' . date('d-m-Y') . '.csv', \Maatwebsite\Excel\Excel::CSV);
        }
        // Se o formato for o XLS.
        if($formato == 'xls'){
            Log::info(Auth::user() . ' exportou a tabela de Lançamentos, através do botão de Exportação ao Fim da Tabela no formato XLS. Filial: ' . Session::get('f_nr_filial') . ' e o ano_mês ' . Session::get('f_ano_mes'));
            return (new VPontoExport)->download('Resumo de Ponto ' . date('d-m-Y') . '.xls', \Maatwebsite\Excel\Excel::XLS);
        }
    }

    /**
     * Rota Lista Funcionários para Folha de Ponto PDF Atual (COM colunas hora extra) - Cosmo
     *
     * @param View
     */
    public function folhaExtra(){
        // Recupera as informações de session.
        $anomes = Session::get('f_ano_mes');
        $filial = Session::get('f_nr_fil');
        $setor = Session::get('setor');

        /**
         * Recupera a(s) filial(ais) do usuário;
         */
        $filiais = (new FilialController)->index();

        /**
         * Recupera o(s) período(s) a serem analisados;
         */
        $periodos = (new FiltersController)->periodos();

        /**
         * Recupera o(s) setor(es) da filial escolhida.
         */
        $setores = (new FiltersController)->setores();

        // -------------------------------------------------------------------
        // Gera o resumo de ponto
        //
        // Garante que novos funcionários sejam incluidos na folha
        // para isso essa procedure deve sempre ser executada pela aplicação
        // antes de mostrar a lista de funcionários e habilitar sua edição
        // -------------------------------------------------------------------

        if(!$this->bloqueado()){
            DB::statement("exec p_gera_ponto @filial = '".$filial."', @anomes = '".$anomes."'");
        }

        // -----------------------------------------------
        // Lista todos os funcionários da folha de ponto
        // e seus respectivos lançamentos
        // -----------------------------------------------

        $funcionarios = Ponto::select('ponto.*', 'SRA010_imp_ponto.*', 'funcionarios.setor')
            ->join('SRA010_imp_ponto', function($join){
                $join->on('SRA010_imp_ponto.matricula', '=', 'ponto.matricula');
                $join->on('SRA010_imp_ponto.nr_fil', '=', 'ponto.nr_fil');
            })
            ->join('funcionarios', 'SRA010_imp_ponto.matricula', '=', 'funcionarios.matricula')
            ->where('ponto.nr_fil', $filial)
            ->where('ponto.ano_mes', $anomes)
            ->orderBy('SRA010_imp_ponto.nome', 'asc');

        if($setor != 0)
        {
            $funcionarios = $funcionarios->where('funcionarios.nr_fil', $filial)
                ->where('funcionarios.setor_id', $setor)
                ->get();
        }else{
            $funcionarios = $funcionarios->get();
        }

        $cod_cidade = $funcionarios->first();

        $status = Status::where('ano_mes', $anomes)
            ->where('nr_fil', $filial)
            ->first();
        $status_lanc = ($status) ? $status->status : 0;

        // Datas

        $mes = substr($anomes, 4, 2);
        $ano = substr($anomes, 0, 4);
        $ano_mes = $ano."-".$mes;

        $start = Carbon::parse($ano_mes)->startOfMonth();
        $end = Carbon::parse($ano_mes)->endOfMonth();

        if(is_null($cod_cidade)){
            $diasMesPonto[] = array(
                'data' => '0',
                'dayOfWeek' => '0',
                'feriado' => false
            );
            return view('folhaextra', compact('filiais', 'periodos', 'setores', 'funcionarios', 'status_lanc', 'diasMesPonto'));
        }

        $feriados = DB::table('FP.dbo.feriados')
            ->whereIn('cidade_id', [0, $cod_cidade->l_cidade_id])
            ->where('data', '>', $start)
            ->get();

        $x = 0;
        $diaPonto = array();

        while($start->lte($end)){
            $isFeriado = false;
            $dates[] = $start->copy();
            $start->addDay();

            if($dates[$x]->dayOfWeek == 0){
                $isFeriado = true;
            }

            foreach($feriados as $folga){
                $folga->data = Carbon::parse($folga->data);
                $feriadox = $folga->data;

                if($folga->data == $dates[$x]){
                    $isFeriado = true;
                }
            }

            //dd($dates[$x]);
            $diasMesPonto[] = array(
                'data' => $dates[$x]->format('d/m/Y'),
                'dayOfWeek' => ucfirst(str_replace("-feira", "", $dates[$x]->dayName)),
                'feriado' => $isFeriado
            );


            $x++;
        }

        Log::info(Auth::user() . ' entrou na páginba de impressão do ponto. Filial: ' . Session::get('f_nr_fil') . ", Setor: " . Session::get('setor') . ', Ano_mes: ' . Session::get('f_ano_mes') . '.');

        return view('folhaextra', compact('filiais', 'periodos', 'setores', 'funcionarios', 'status_lanc', 'diasMesPonto'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use App\Fechamentos;
use App\User;

use App\Exports\FechamentosExport;
use App\Exports\TransferidosExport;

use App\Http\Controllers\FiltersController;

class FechamentosController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        // Autoria o usuário.
        $this->authorize('eAdministrador', User::class);

        // Recupera variável de sessão.
        $anomes = Session::get('f_ano_mes');

        // Executa query de busca dos dados
        $qry = Fechamentos::select(DB::raw('max(data_fechamento) as data_fechamento'))
            ->where('ano_mes', '=', $anomes)
            ->first();

        // Ajusta a data fechamento.
        $data_fechamento = $qry->data_fechamento ? $qry->data_fechamento : NULL;

        /**
         * Recupera o(s) período(s) a serem analisados;
         */
        $periodos = (new FiltersController)->periodos();

        // Registra Log de acesso.
        Log::info(Auth::user()->usuario . ' acessou a tela de fechametnos. Usando o perído: ' . Session::get('f_ano_mes') . '.');

        // Retorna a view.
        return view('fechamentos', compact('data_fechamento', 'periodos'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param String $formato
     * @return Excel
     */
    public function exportaFechamentos($formato){
        // Autoria o usuário.
        $this->authorize('eAdministrador', User::class);

        // Verifica o formato
        if(($formato != 'csv') && ($formato != 'xls')){
            return response(json_encode(['message' => 'Formato Inválido']), 403);
        }

        // Se o formato for o CSV.
        if($formato == 'csv'){
            Log::info(Auth::user() . ' exportou a tabela de Fechamentos. Ano_mes: ' . Session::get('f_ano_mes'));
            return (new FechamentosExport)->download('Fechamentos ' . date('d-m-Y') . '.csv', \Maatwebsite\Excel\Excel::CSV);
        }
        // Se o formato for o XLS.
        if($formato == 'xls'){
            Log::info(Auth::user() . ' exportou a tabela de Fechamentos. Ano_mes: ' . Session::get('f_ano_mes'));
            return (new FechamentosExport)->download('Fechamentos ' . date('d-m-Y') . '.xls', \Maatwebsite\Excel\Excel::XLS);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param String $formato
     * @return Excel
     */
    public function exportaTransferidos($formato){
        // Autoria o usuário.
        $this->authorize('eAdministrador', User::class);

        // Verifica o formato
        if(($formato != 'csv') && ($formato != 'xls')){
            return response(json_encode(['message' => 'Formato Inválido']), 403);
        }

        // Se o formato for o CSV.
        if($formato == 'csv'){
            Log::info(Auth::user() . ' exportou os Transferidos. Ano_mes: ' . Session::get('f_ano_mes'));
            return (new TransferidosExport)->download('Transferidos ' . date('d-m-Y') . '.csv', \Maatwebsite\Excel\Excel::CSV);
        }
        // Se o formato for o XLS.
        if($formato == 'xls'){
            Log::info(Auth::user() . ' exportou os Transferidos. Ano_mes: ' . Session::get('f_ano_mes'));
            return (new TransferidosExport)->download('Transferidos ' . date('d-m-Y') . '.xls', \Maatwebsite\Excel\Excel::XLS);
        }
    }
}

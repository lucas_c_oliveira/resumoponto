<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Carbon\Carbon;

use App\User;
use App\Ponto;
use App\Funcionarios;

class FiltersController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Altera o cookie de filial de sessão
     *
     * @return Redirect
     */
    public function filtrar_por_filial(Request $request){

        if (Auth::user()->can('eAdministrador', User::class) ||
            Auth::user()->can('eSupervisor', User::class) ||
            Auth::user()->can('eGerenteCDI', User::class)){

            $this->validate($request, [
                'nr_fil' => 'required|integer|exists:filiais,nr_fil',
            ], [
                'nr_fil.required' => 'O número da filial é obirgatório.',
                'nr_fil.integer' => 'O número da filial deve ser um número inteiro.',
                'nr_fil.exists' => 'O número da filial é inválido.'
            ]);

            Session::put('f_nr_fil', $request->input('nr_fil'));
            Session::put('setor', '0');

            return back();

        }else{
            return back();
        }
    }

    /**
     * Retorna os períodos disponíveis para configuração.
     *
     * @return Illuminate\Support\Collection
     */
    public function periodos(){

        Carbon::setLocale('pt_BR');

        $periodos = Ponto::select('ano_mes')
            ->distinct('ano_mes')
            ->orderBy('ano_mes', 'desc')
            ->get();

        foreach($periodos as $p){
            $p->ano = substr($p->ano_mes, 0, 4);
            $p->mes = substr($p->ano_mes, 4, 2);
            $p->mes_extenso = ucfirst(Carbon::create($p->ano, $p->mes)->isoFormat('MMMM'));
        }

        $ano_mes_sessao = Session::get('f_ano_mes');
        $ano_atual = substr($ano_mes_sessao, 0, 4);
        $mes_atual = substr($ano_mes_sessao, 4, 2);

        $periodo_selecionado = Collection::make([
            'ano_mes' => $ano_mes_sessao,
            'ano' => $ano_atual,
            'mes' => $mes_atual,
            'mes_extenso' => ucfirst(Carbon::create($ano_atual, $mes_atual)->isoFormat('MMMM'))
        ]);

        $ano_periodo_passado = $ano_atual;
        $mes_periodo_passado = $mes_atual;

        if((int)$mes_periodo_passado >= 2 && (int)$mes_periodo_passado <= 12){
            $mes_periodo_passado = (int)$mes_periodo_passado - 1;
        }else{
            $mes_periodo_passado = 12;
            $ano_periodo_passado = (int)$ano_periodo_passado - 1;
        }

        $perido_passado = Collection::make([
            'ano_mes' => $ano_periodo_passado.$mes_periodo_passado,
            'ano' => $ano_periodo_passado,
            'mes' => $mes_periodo_passado,
            'mes_extenso' => ucfirst(Carbon::create($ano_periodo_passado, $mes_periodo_passado)->isoFormat('MMMM'))
        ]);

        return Collection::make(['periodos' => $periodos, 'periodo_selecionado' => $periodo_selecionado, 'periodo_passado' => $perido_passado]);
    }

    /**
     * Altera o cookie de ano e mes;
     *
     * @return Redirect
     */
    public function filtrar_por_periodo(Request $request){

        $this->validate($request, [
            'ano_mes' => 'required|integer|digits:6',
        ], [
            'ano_mes.required' => 'O ano e mês são obrigatórios',
            'ano_mes.integer' => 'O ano e mês devem ser um inteiro.',
            'ano_mes.size' => 'O ano e mês devem possuir 6 caracteres exatamente.',
        ]);

        Session::put('f_ano_mes', $request->input('ano_mes'));

        return back();
    }

    /**
     * Retorna os setores disponíveis para configuração.
     *
     * @return Array $setores
     */
    public function setores(){
        // Declara vetor de setores;
        $setores = array();
        $filial = Session::get('f_nr_fil');
        // Recupera os setores únicos da filial
        $result = Funcionarios::select('setor', 'setor_id')
            ->where('nr_fil', $filial)
            ->distinct()
            ->orderBy('setor', 'asc')
            ->get();

        $result->prepend(new Funcionarios(['setor' => 'Todos os setores', 'setor_id' => '0']));

        $setor = Session::get('setor');

        if($setor == 0){
            $setor_selecionado = array('setor_id' => $setor, 'setor' => 'Todos os setores');
        }else{
            $setor_selecionado = Funcionarios::where('setor_id', $setor)
                ->select('setor_id', 'setor')
                ->first();
        }

        return Collection::make(['setores' => $result, 'setor_selecionado' => $setor_selecionado]);
    }

    /**
     * Altera o cookie de setor;
     *
     * @param Illuminate\Http\Request $request
     * @return Redirect
     */
    public function filtrar_por_setor(Request $request){
        $this->validate($request, [
            'setor_id' => 'required|integer',
        ], [
            'setor_id.required' => 'O setor é obrigatório.',
            'setor_id.integer' => 'O setor deve ser um número inteiro.'
        ]);

        if($request->input('setor_id') >= 0){
            Session::put('setor', $request->setor_id);
        }else{
            Session::put('setor', '0');
        }

        return back();
    }
}

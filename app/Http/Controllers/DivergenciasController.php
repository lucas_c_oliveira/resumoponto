<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Divergencias;
use App\VDivergencias;
use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\FiltersController;

class DivergenciasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // Recuperação de variáveis  de sessão.
        $anomes = Session::get('f_ano_mes');
        $filial = Session::get('f_nr_fil');

        // Executa essa chamada de procedure para gerar as divergências.
        DB::statement("exec p_gera_divergencias @ano_mes = '" . $anomes . "'");

        // Recupera as Divergências a partir de uma View.
        if(Auth::user()->can('eAdministrador', User::class)){
            $divergencias = VDivergencias::where('ano_mes', $anomes)
                ->join('filiais', 'filiais.nr_fil', '=', 'v_divergencias.nr_fil')
                ->orderBy('regiao_id', 'asc')
                ->orderBy('v_divergencias.nr_fil', 'asc')
                ->orderBy('setor', 'asc')
                ->get();
        }else{
            $divergencias = VDivergencias::where('ano_mes', $anomes)
                ->join('filiais', 'filiais.nr_fil', '=', 'v_divergencias.nr_fil')
                ->where('v_divergencias.nr_fil', $filial)
                ->orderBy('v_divergencias.nr_fil', 'asc')
                ->orderBy('setor', 'asc')
                ->get();
        }

        // Recupera os períodos dos filtros.
        $periodos = (new FiltersController)->periodos();

        // Registra nos Logs.
        Log::info(Auth::user() . ' acessou a rota de divergências (Home) com filial: ' . Session::get('f_nr_fil') . ' e com ano_mes ' . Session::get('f_ano_mes') . '.');

        return view('divergencias.index', compact('divergencias', 'periodos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // Recupera a divergência
        $divergencia = VDivergencias::find($id);
        // Recupera as demais divergências do período
        $divergencias_periodo = VDivergencias::where('setfil_id', $divergencia->setfil_id)
            ->where('ano_mes', '<=', $divergencia->ano_mes)
            ->orderBy('ano_mes', 'desc')
            ->limit(12)
            ->get();
        // Registra o Log.
        Log::info(Auth::user() . ' recuperou a divergência (SHOW) '. $id .' a partir da filial ' . Session::get('f_nr_filial') . ' e do ano_mês' . Session::get('f_ano_mes') . '.');
        // Retorna para view de visualização das divergências
        return view('divergencias.show', compact('divergencia', 'divergencias_periodo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // Recupera a divergência
        $divergencia = VDivergencias::find($id);
        // Registra o Log.
        Log::info(Auth::user() . ' recuperou a divergência (EDIT) '. $id .' a partir da filial ' . Session::get('f_nr_filial') . ' e do ano_mês' . Session::get('f_ano_mes') . '.');
        // Envia a divergência para edição.
        return view('divergencias.edit', compact('divergencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // Valida a alteração da divergência
        $this->validate($request, [
            'observacao' => 'required|string|min:3',
            'ponto' => 'required|string|size:1',
            'holerite' => 'required|string|size:1',
        ],[
            'observacao.required' => 'A Observação é obrigatória.',
            'observacao.string' => 'A Observação deve ser alfanumérica.',
            'observacao.min' => 'A Observação deve possuir no mínimo 3 caracteres.',
            'ponto.required' => 'O Ponto é obrigatório.',
            'ponto.string' => 'O Ponto deve ser um caractere.',
            'ponto.size' => 'O Ponto deve ser somente um caractere.',
            'holerite.required' => 'O Holerite é obrigatório',
            'holerite.string' => 'O Holerite deve ser um caractere.',
            'holerite.size' => 'O Holerite deve ser somente um caractere',
        ]);

        // Recupera a divergência
        $divergencia = Divergencias::find($id);
        $divergencia->update([
            'observacao' => $request->observacao,
            'ponto' => $request->ponto,
            'holerite' => $request->holerite,
            'usuario' => Auth::user()->usuario,
            'ult_alteracao' => DB::raw('getdate()')
        ]);

        // Se estiver tudo certo atualiza a data de entrega.
        if(is_null($divergencia->dt_entrega)){
            if($request->ponto != 'P' || $request->holerite != 'P'){
                $divergencia->update([
                    'dt_entrega' => DB::raw('getdate()')
                ]);
            }
        }

        // Registra o Log.
        Log::info(Auth::user() . ' editou a divergência '. $id .' a partir da filial ' . Session::get('f_nr_filial') . ' e do ano_mês' . Session::get('f_ano_mes') . '.');

        return back()->with(['success' => '<i class="fas fa-exclamation"></i> Não conformidade salva com sucesso.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return abort(404);
    }
}

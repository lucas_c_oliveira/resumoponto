<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\FiltersController;

class TotaisController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        // Autoriza o usuário.
        $this->authorize('eAdministrador', User::class);

        // Recuperando variável de sessão.
        $anomes = Session::get('f_ano_mes');

        // Recupera todos os totais.
        $totais = DB::select('exec p_totais :anomes', ['anomes' => $anomes]);

        /**
         * Recupera o(s) período(s) a serem analisados;
         */
        $periodos = (new FiltersController)->periodos();

        // Registra Log de acesso.
        Log::info(Auth::user() . ' acessou a tela de totais. Usando o perído: ' . Session::get('f_ano_mes') . '.');

        // Retorna a view de totais
        return view('totais', compact('totais', 'periodos'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function exporta(){

        // Autoriza o usuário.
        $this->authorize('eAdministrador', User::class);

        // Recupera variáveis de sessão.
        $anomes = Session::get('f_ano_mes');

        // Recupera as filiais via procedure.
        $filiais = DB::select(DB::raw('exec p_totais @anomes = ' . $anomes));

        // Transforma as filiais em um vetor.
        $filiais = json_decode(json_encode($filiais), true);

        // Declara a saída.
        $output = '';

        // Se houver algo crie o array.
        if (isset($filiais[0])) {
            $cols = array_keys($filiais[0]);
            $output .=  implode(";", $cols) . "\n";
        }

        // Para cada linha, transforme os dados.
        foreach ($filiais as $row) {
            $output .=  implode(";", $row) . "\n";
        }

        // Define os readers da response.
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="totais.csv"',
        );

        // Registra Log de acesso.
        Log::info(Auth::user() . ' exportou a tabela de totais. Usando o perído: ' . Session::get('f_ano_mes') . '.');

        // Envia o arquivo.
        return response()->make(rtrim($output, "\n"), 200, $headers);
    }
}

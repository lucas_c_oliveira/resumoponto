<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

use App\Fechamentos;

use Illuminate\Support\Facades\Session;

class FechamentosExport implements FromCollection, WithHeadings
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $anomes = Session::get('f_ano_mes');

        $fechamentos = Fechamentos::where('ano_mes', '=', $anomes)
                ->get();

        return $fechamentos;
    }

    public function headings(): array{
        return [
            'nr_fil',
            'matricula',
            'nome',
            'ano_mes',
            'dias_ferias',
            'dias_atestado',
            'horas_noturnas',
            'feriados',
            'faltas',
            'horas_extras',
            'domingos',
            'horas_noturnass_ma',
            'feriados_ma',
            'faltas_ma',
            'horas_extras_ma',
            'domingos_ma',
            'v_horas_noturnas',
            'v_feriados',
            'v_faltas',
            'v_horas_extras',
            'v_domingos',
            'v_horas_noturnas_ma',
            'v_feriados_ma',
            'v_domingos_ma',
            'data_fechamento',
        ];
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class TransferidosExport implements FromCollection
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        $anomes = Session::get('f_ano_mes');

        $transferidos = DB::table('v_ponto_imp_protheus')
            ->where('ano_mes', $anomes)
            ->whereIn('situacao', ['D', 'T'])
            ->get();

        return $transferidos;
    }
}

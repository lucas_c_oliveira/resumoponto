<?php

namespace App\Exports;

use App\VPonto;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Illuminate\Support\Facades\Session;

class VPontoExport implements FromCollection, WithHeadings
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $anomes = Session::get('f_ano_mes');
        $filial = Session::get('f_nr_fil');

        $funcionarios = VPonto::select('*')
            ->where('ano_mes', '=', $anomes)
            ->where('nr_fil', '=', $filial)
            ->orderBy('nome', 'asc')
            ->get();

        return $funcionarios;
    }

    public function headings(): array
    {
        return [
            'id',
            'matricula',
            'nome',
            'nr_fil',
            'catfunc',
            'sindica',
            'situacao',
            'ano_mes',
            'dias_ferias',
            'dias_atestado',
            'faltas_ma',
            'horas_extras_ma',
            'horas_noturnas',
            'v_horas_noturnas',
            'feriados',
            'v_feriados',
            'faltas',
            'v_faltas',
            'horas_extras',
            'v_horas_extras',
            'horas_noturnas_ma',
            'v_horas_noturnas_ma',
            'feriados_ma',
            'v_feriados_ma',
            'domingos',
            'v_domingos',
            'domingos_ma',
            'v_domingos_ma',
            'obs',
        ];
    }

}

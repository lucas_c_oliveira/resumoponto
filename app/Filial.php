<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filial extends Model
{
    //
    protected $table = 'filiais';

    protected $primaryKey = 'nr_fil';

    /**
     * Quando a tabela for uma view,
     * deve-se desativar a incrementação.
     */
    public $incrementing = false;

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $fillable = [
        'nr_fil',
        'filial',
        'email',
        'regiao_id',
        'regiao'
    ];
}

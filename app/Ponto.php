<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ponto extends Model
{
    //
    protected $table = 'ponto';

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $fillable = [
        'id',
        'matricula',
        'nr_fil',
        'ano_mes',
        'dias_ferias',
        'dias_atestado',
        'horas_noturnas',
        'feriados',
        'faltas',
        'horas_extras',
        'domingos',
        'horas_noturnas_ma',
        'feriados_ma',
        'faltas_ma',
        'horas_extras_ma',
        'domingos_ma',
        'obs'
    ];
}

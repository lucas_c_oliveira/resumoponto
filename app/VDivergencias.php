<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VDivergencias extends Model
{
    //
    protected $table = 'v_divergencias';

    protected $primaryKey = 'id';

    /**
     * Quando a tabela for uma view,
     * deve-se desativar a incrementação.
     */
    public $incrementing = false;

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $fillable = [
        'nr_fil',
        'filial',
        'setor',
        'id',
        'setfil_id',
        'ano_mes',
        'ponto',
        'holerite',
        'observacao',
        'dt_entrega',
        'ult_alteracao',
        'usuario',
    ];


}

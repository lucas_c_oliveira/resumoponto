<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionarios extends Model
{
    //
    protected $table = 'funcionarios';

    protected $primaryKey = 'matricula';

    /**
     * Quando a tabela for uma view,
     * deve-se desativar a incrementação.
     */
    public $incrementing = false;

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $fillable = [
        'matricula',
        'nome',
        'nr_fil',
        'filial',
        'setor_id',
        'setor',
        'cargo',
        'regiao',
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fechamentos extends Model
{
    //
    protected $table = 'fechamentos';

    protected $primaryKey = array('nr_fil', 'matricula', 'ano_mes');

    /**
     * Quando a chave primária for composta,
     * deve-se desativar a incrementação.
     */
    public $incrementing = false;

    /**
     * Quando não houver timestamps registre isso.
     */
    public $timestamps = false;

    protected $fillable = [
        'nr_fil',
        'matricula',
        'nome',
        'ano_mes',
        'dias_ferias',
        'dias_atestado',
        'horas_noturnas',
        'feriados',
        'faltas',
        'horas_extras',
        'domingos',
        'horas_noturnass_ma',
        'feriados_ma',
        'faltas_ma',
        'horas_extras_ma',
        'domingos_ma',
        'v_horas_noturnas',
        'v_feriados',
        'v_faltas',
        'v_horas_extras',
        'v_domingos',
        'v_horas_noturnas_ma',
        'v_feriados_ma',
        'v_domingos_ma',
        'data_fechamento',
    ];
}

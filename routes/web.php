<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes([
    'register' => false, // Remove rota de registro.
    'reset' => false, // Remove rota de recuperação de senha.
    'verify' => false, // Remove rota de verificação de e-mails.
]);


/**
 * Grupo de rotas com Autenticação
 */
Route::group(['prefix' => '/', 'middleware' => 'auth'], function() {
    // Grupo de Rotas dos Lançamentos de Ponto
    Route::resource('/', 'PontoController')->name('home', [
        'index' => '/',
        'store' => '/'
    ]);
    Route::get('/exporta/lancamentos/{formato}', 'PontoController@exportaLancamentos');
    // Grupo de Rotas dos usuários.
    Route::resource('/usuarios', 'UserController');
    // Grupo de Rotas dos Filtros.
    Route::get('/filtrar_por_filial', 'FiltersController@filtrar_por_filial');
    Route::get('/filtrar_por_periodo', 'FiltersController@filtrar_por_periodo');
    Route::get('/filtrar_por_setor', 'FiltersController@filtrar_por_setor');
    // Grupo de Rotas das Divergências
    Route::resource('/divergencias', 'DivergenciasController');
    // Grupo de Rotas de Totais
    Route::get('/totais', 'TotaisController@index');
    Route::get('/exporta/totais', 'TotaisController@exporta');
    // Grupo de Rotas de Fechamentos
    Route::get('/fechamentos', 'FechamentosController@index');
    Route::get('/exporta/fechamentos/{formato}', 'FechamentosController@exportaFechamentos');
    Route::get('/exporta/transferidos/{formato}', 'FechamentosController@exportaTransferidos');
    // Grupo de Rotas de Folha Extra
    Route::get('/folhaextra', 'PontoController@folhaextra');
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFechamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fechamentos', function (Blueprint $table) {
            $table->integer('nr_fil')->unsigned();
            $table->integer('matricula')->unsigned();
            $table->string('nome', 30);
            $table->integer('ano_mes')->unsigned();
            $table->integer('dias_ferias')->unsigned()->nullable();
            $table->integer('dias_atestado')->unsigned()->nullable();
            $table->decimal('horas_noturnas', 12, 2)->nullable();
            $table->integer('faltas')->unsigned()->nullable();
            $table->decimal('horas_extras', 12, 2)->nullable();
            $table->integer('domingos')->unsigned()->nullable();
            $table->decimal('horas_noturnas_ma', 12, 2)->nullable();
            $table->integer('feriados_ma')->unsigned()->nullable();
            $table->integer('faltas_ma')->unsigned()->nullable();
            $table->decimal('horas_extras_ma', 12, 2)->nullable();
            $table->integer('domingos_ma')->unsigned()->nullable();
            $table->integer('v_horas_noturnas')->unsigned();
            $table->integer('v_feriadas')->unsigned();
            $table->integer('v_faltas')->unsigned();
            $table->integer('v_horas_extras')->unsigned();
            $table->integer('v_domingos')->unsigned();
            $table->integer('v_horas_noturnas_ma')->unsigned();
            $table->integer('v_domingos_ma')->unsigned();
            $table->dateTime('data_fechamento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fechamentos');
    }
}

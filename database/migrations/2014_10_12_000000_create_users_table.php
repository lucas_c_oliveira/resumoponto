<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            // Identificação e chave primária da tabela.
            $table->increments('id');
            // Coluna responsável por guardar chave de acesso do ADLDAP
            $table->string('objectguid')->nullable();
            // Nome do usuário.
            $table->string('nome');
            // Usuário AD do usuário.
            $table->string('usuario')->unique();
            // Matrícula do usuário.
            $table->string('matricula', 6)->nullable();
            // Cargo do usuário.
            $table->string('cargo', 100)->nullable();
            // Número da filial onde o usuário atua.
            $table->integer('nr_fil')->nullable();
            // Setor do usuário.
            $table->string('setor', 100)->nullable();
            // Verificação de e-mail cancelada, pois atua com username.
            // $table->timestamp('email_verified_at')->nullable();
            // Senha do usuário
            $table->string('password');
            // Remember Token cancelado pois não há na tabela.
            // $table->rememberToken();
            // Carimbo de data hora de registro e alteração do usuário.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePontoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ponto', function (Blueprint $table) {
            $table->increments('id');
            // Número da matrícula do funcionário.
            $table->integer('matricula')->unsigned();
            // Número da filial do funcionário.
            $table->integer('nr_fil')->unsigned();
            // Ano e mês do registro do ponto.
            $table->integer('ano_mes')->unsigned();
            // Quantidade de dias de férias no Ano e Mês vigente.
            $table->integer('dias_ferias')->unsigned()->nullable();
            // Quantidade de dias de atestado no Ano e Mês vigente.
            $table->integer('dias_atestado')->unsigned()->nullable();
            // Quantidade de horas e minutos de horas noturnas foram trabalhadas no Ano e Mês vigente.
            $table->string('horas_noturnas', 5)->nullable();
            // Quantidade de feriados trabalhados no Ano e Mês vigente.
            $table->integer('feriados')->unsigned()->nullable();
            // Quantidade de faltas tidas pelo funcionário no Ano e Mês vigente.
            $table->integer('faltas')->unsigned()->nullable();
            // Quantidade de horas extras trabalhadas pelo funcionário no Ano e Mês vigente.
            $table->string('horas_extras', 5)->nullable();
            // Qunatidade de domingos trabalhados pelo funcionário no Ano e Mês vigente.
            $table->integer('domingos')->unsigned()->nullable();
            // Quantidade de horas_noturnas trabalhadas pelo funcionário no Ano e Mês anterior.
            $table->string('horas_noturnas_ma', 5)->nullable();
            // Quantidade de feriados trabalhados pelo funcionário no Ano e Mês anterior.
            $table->integer('feriados_ma', 100)->nullable();
            // Quantidade de faltas tidas pelo funcionário no Ano e Mês anterior.
            $table->integer('faltas_ma')->unsigned()->nullable();
            // Quantidade de horas_extras trabalhadas pelo funcionário no Ano e Mês anterior.
            $table->string('horas_extras_ma', 5)->nullable();
            // Quantidade de domingos trabalhados pelo funcionário no Ano e Mês anterior.
            $table->integer('domingos_ma')->unsigned()->nullable();
            // Observações
            $table->text('obs')->nullable();
            // Carimbos de data e hora não são registrados nessa tabela. E nem em outro lugar algum.
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ponto');
    }
}

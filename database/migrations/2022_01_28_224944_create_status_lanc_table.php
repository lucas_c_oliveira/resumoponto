<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusLancTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_lanc', function (Blueprint $table) {
            // Ano e Mês do status;
            $table->integer('ano_mes')->unsigned();
            // Número da filial do status;
            $table->integer('nr_fil')->unsigned();
            // Status
            $table->integer('status')->unsigned()->nullable();
            // Carimbo de data hora não está presente na tabela.
            // $table->timestamps();
            // Definição de chave primária.
            $table->primary(array('ano_mes', 'nr_fil'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_lanc');
    }
}
